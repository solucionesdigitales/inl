<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
  <style>
  h1{
    text-align: center;
    text-transform: uppercase;
  }

  body {
    margin: 0;
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #212529;
    text-align: left;
    background-color: #fff;
  }
  td {
    page-break-inside: avoid;
  }
  table {
    page-break-inside: avoid !important;
  }
  html{margin:40px 50px}
  .contenido{
    font-size: 20px;
  }
  .badge {
    border: 1px solid #000;
  }
  .table {
    width: 100%;
    margin-bottom: 1rem;
    color: #212529;
  }

  .table th,
  .table td {
    padding: 0.75rem;
    vertical-align: top;
    border-top: 1px solid #dee2e6;
  }

  .table thead th {
    vertical-align: bottom;
    border-bottom: 2px solid #dee2e6;
  }

  .table tbody + tbody {
    border-top: 2px solid #dee2e6;
  }
  .table {
    border-collapse: collapse !important;
  }
  .table td,
  .table th {
    background-color: #fff !important;
  }
  .table-bordered th,
  .table-bordered td {
    border: 1px solid #dee2e6 !important;
  }
  .table-dark {
    color: inherit;
  }
  .table-dark th,
  .table-dark td,
  .table-dark thead th,
  .table-dark tbody + tbody {
    border-color: #dee2e6;
  }
  .table .thead-dark th {
    color: inherit;
    border-color: #dee2e6;
  }


  .table-responsive {
    display: block;
    width: 100%;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;
  }

  .table-responsive > .table-bordered {
    border: 0;
  }

  #primero{
    background-color: #ccc;
  }
  #segundo{
    color:#44a359;
  }
  #tercero{
    text-decoration:line-through;
  }
  .content {
    padding: 1% 3%;
    /* margin: 0 -4%; */
  }
  .list-one {
    font-size: 12px;
  }
  .p-one {
    font-size: 12px;
  }
  .tac{
    text-align: center;
  }
  .card {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid rgba(0, 0, 0, 0.125);
    border-radius: 0.25rem;
  }

  .card > hr {
    margin-right: 0;
    margin-left: 0;
  }

  .card > .list-group:first-child .list-group-item:first-child {
    border-top-left-radius: 0.25rem;
    border-top-right-radius: 0.25rem;
  }

  .card > .list-group:last-child .list-group-item:last-child {
    border-bottom-right-radius: 0.25rem;
    border-bottom-left-radius: 0.25rem;
  }

  .card-body {
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    padding: 1.25rem;
  }

  .card-header {
    padding: 0.75rem 1.25rem;
    margin-bottom: 0;
    background-color: rgba(0, 0, 0, 0.03);
    border-bottom: 1px solid rgba(0, 0, 0, 0.125);
  }

  .card-header:first-child {
    border-radius: calc(0.25rem - 1px) calc(0.25rem - 1px) 0 0;
  }

  .card-header + .list-group .list-group-item:first-child {
    border-top: 0;
  }


  hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid rgba(0, 0, 0, 0.1);
  }

  h1, h2, h3, h4, h5, h6 {
    margin-top: 0;
    margin-bottom: 0.5rem;
  }

  h6, .h6 {
    font-size: 1rem;
  }

  p {
    margin-top: 0;
    margin-bottom: 1rem;
  }

  .badge {
    display: inline-block;
    padding: 0.25em 0.4em;
    font-size: 75%;
    font-weight: 700;
    line-height: 1;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0.25rem;
    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  }

  .badge {
    border: 1px solid #17a2b8;
  }

  .badge-info {
    color: #fff;
    background-color: #17a2b8;
  }

  .badge-secondary {
    color: #fff;
    background-color: #6c757d;
    border: 1px solid #6c757d;
  }

  a.badge-secondary:hover, a.badge-secondary:focus {
    color: #fff;
    background-color: #545b62;
  }

  a.badge-secondary:focus, a.badge-secondary.focus {
    outline: 0;
    box-shadow: 0 0 0 0.2rem rgba(108, 117, 125, 0.5);
  }




  </style>
</head>
<body>
  <div class="content">

    <div class="card" style="page-break-after: always;top:50%;">
      <div class="card-body">
        <h1> <b> Reporte Ejecutivo de {{$state}}</b></h1>
      </div>
    </div>

    <div class="card" style="page-break-after: always;">
      <div class="card-header">
        <h5> Ámbito Regulatorio</h5>
      </div>
      <div class="card-body" style="padding: 0;">

        <div class="card" style="">
          <div class="card-header" style="background:#13385d;color:white;">
            Criterio 1: Aplicación de manuales y/o protocolos
          </div>
          <div class="card-body">
            @foreach ($criterion_conclusion as $key => $value)
              @if($value->criterion_id == 1)
              {!! $value->problematica_conclusion !!}
              @endif
            @endforeach
          </div>
        </div>
        <div class="card" style="page-break-inside: avoid;">
          <div class="card-header" style="background:#13385d;color:white;">
            Criterio 2: Política de persecución penal
          </div>
          <div class="card-body">
            @foreach ($criterion_conclusion as $key => $value)
            @if($value->criterion_id == 2)
            {!! $value->problematica_conclusion !!}
            @endif
            @endforeach
          </div>
        </div>
        <div class="card" style="page-break-inside: avoid;">
          <div class="card-header" style="background:#13385d;color:white;">
            Criterio 3: Interpretación de normas jurídicas
          </div>
          <div class="card-body">
            @foreach ($criterion_conclusion as $key => $value)
            @if($value->criterion_id == 3)
            {!! $value->problematica_conclusion !!}
            @endif
            @endforeach
          </div>
        </div>
      </div>
    </div>

    <div class="card" style="page-break-after: always;">
      <div class="card-header">
        <h5> Ámbito Institucional</h5>
      </div>
      <div class="card-body" style="padding:0;">

        <div class="card" style="">
          <div class="card-header" style="background:#13385d;color:white;">
            Criterio 4: Estructura organizacional
          </div>
          <div class="card-body">
            @foreach ($criterion_conclusion as $key => $value)
              @if($value->criterion_id == 4)
              {!! $value->problematica_conclusion !!}
              @endif
            @endforeach
          </div>
        </div>
        <div class="card" style="page-break-inside: avoid;">
          <div class="card-header" style="background:#13385d;color:white;">
            Criterio 5: Autonomía del Agente del Ministerio Público
          </div>
          <div class="card-body">
            @foreach ($criterion_conclusion as $key => $value)
              @if($value->criterion_id == 5)
              {!! $value->problematica_conclusion !!}
              @endif
            @endforeach
          </div>
        </div>
        <div class="card" style="page-break-inside: avoid;">
          <div class="card-header" style="background:#13385d;color:white;">
            Criterio 6: Instrucciones por parte del superior y/o prácticas no documentadas
          </div>
          <div class="card-body">
            @foreach ($criterion_conclusion as $key => $value)
              @if($value->criterion_id == 6)
              {!! $value->problematica_conclusion !!}
              @endif
            @endforeach
          </div>
        </div>
        <div class="card" style="page-break-inside: avoid;">
          <div class="card-header" style="background:#13385d;color:white;">
            Criterio 7: Supervisión
          </div>
          <div class="card-body">
            @foreach ($criterion_conclusion as $key => $value)
              @if($value->criterion_id == 7)
              {!! $value->problematica_conclusion !!}
              @endif
            @endforeach
          </div>
        </div>
        <div class="card" style="page-break-inside: avoid;">
          <div class="card-header" style="background:#13385d;color:white;">
            Criterio 8: Coordinación al interior de la Procuraduría o Fiscalía
          </div>
          <div class="card-body">
            @foreach ($criterion_conclusion as $key => $value)
              @if($value->criterion_id == 8)
              {!! $value->problematica_conclusion !!}
              @endif
            @endforeach
          </div>
        </div>
        <div class="card" style="page-break-inside: avoid;">
          <div class="card-header" style="background:#13385d;color:white;">
            Criterio 9: Coordinación con otras instituciones
          </div>
          <div class="card-body">
            @foreach ($criterion_conclusion as $key => $value)
              @if($value->criterion_id == 9)
              {!! $value->problematica_conclusion !!}
              @endif
            @endforeach
          </div>
        </div>
        <div class="card" style="page-break-inside: avoid;">
          <div class="card-header" style="background:#13385d;color:white;">
            Criterio 10: Capacitación
          </div>
          <div class="card-body">
            @foreach ($criterion_conclusion as $key => $value)
              @if($value->criterion_id == 10)
              {!! $value->problematica_conclusion !!}
              @endif
            @endforeach
          </div>
        </div>
        <div class="card" style="page-break-inside: avoid;">
          <div class="card-header" style="background:#13385d;color:white;">
            Criterio 11: Evaluación del desempeño
          </div>
          <div class="card-body">
            @foreach ($criterion_conclusion as $key => $value)
              @if($value->criterion_id == 11)
              {!! $value->problematica_conclusion !!}
              @endif
            @endforeach
          </div>
        </div>

      </div>
    </div>

    <div class="card" style="page-break-after: always;">
      <div class="card-header">
        <h5> Ámbito Individual</h5>
      </div>
      <div class="card-body" style="padding:0;">

        <div class="card" style="">
          <div class="card-header" style="background:#13385d;color:white;">
            Criterio 12: Incentivos y sanciones
          </div>
          <div class="card-body">
            @foreach ($criterion_conclusion as $key => $value)
              @if($value->criterion_id == 12)
              {!! $value->problematica_conclusion !!}
              @endif
            @endforeach
          </div>
        </div>
        <div class="card" style="page-break-inside: avoid;">
          <div class="card-header" style="background:#13385d;color:white;">
            Criterio 13: Rol de otros actores
          </div>
          <div class="card-body">
            @foreach ($criterion_conclusion as $key => $value)
              @if($value->criterion_id == 13)
              {!! $value->problematica_conclusion !!}
              @endif
            @endforeach
          </div>
        </div>
        <div class="card" style="page-break-inside: avoid;">
          <div class="card-header" style="background:#13385d;color:white;">
            Criterio 14: Percepción pública
          </div>
          <div class="card-body">
            @foreach ($criterion_conclusion as $key => $value)
              @if($value->criterion_id == 14)
              {!! $value->problematica_conclusion !!}
              @endif
            @endforeach
          </div>
        </div>
        <div class="card" style="page-break-inside: avoid;">
          <div class="card-header" style="background:#13385d;color:white;">
            Criterio 15: Interiorización de principios y valores
          </div>
          <div class="card-body">
            @foreach ($criterion_conclusion as $key => $value)
              @if($value->criterion_id == 15)
              {!! $value->problematica_conclusion !!}
              @endif
            @endforeach
          </div>
        </div>

      </div>
    </div>


  </div>
</body>
</html>
