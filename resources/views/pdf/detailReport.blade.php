<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
  <style>
  h1{
    text-align: center;
    text-transform: uppercase;
  }

  body {
    margin: 0;
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #212529;
    text-align: left;
    background-color: #fff;
  }
  td {
    page-break-inside: avoid;
  }
  table {
    page-break-inside: avoid !important;
  }
  html{margin:40px 50px}
  .contenido{
    font-size: 20px;
  }
  .badge {
    border: 1px solid #000;
  }
  .table {
    width: 100%;
    margin-bottom: 1rem;
    color: #212529;
  }

  .table th,
  .table td {
    padding: 0.75rem;
    vertical-align: top;
    border-top: 1px solid #dee2e6;
  }

  .table thead th {
    vertical-align: bottom;
    border-bottom: 2px solid #dee2e6;
  }

  .table tbody + tbody {
    border-top: 2px solid #dee2e6;
  }
  .table {
    border-collapse: collapse !important;
  }
  .table td,
  .table th {
    background-color: #fff !important;
  }
  .table-bordered th,
  .table-bordered td {
    border: 1px solid #dee2e6 !important;
  }
  .table-dark {
    color: inherit;
  }
  .table-dark th,
  .table-dark td,
  .table-dark thead th,
  .table-dark tbody + tbody {
    border-color: #dee2e6;
  }
  .table .thead-dark th {
    color: inherit;
    border-color: #dee2e6;
  }


  .table-responsive {
    display: block;
    width: 100%;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;
  }

  .table-responsive > .table-bordered {
    border: 0;
  }

  #primero{
    background-color: #ccc;
  }
  #segundo{
    color:#44a359;
  }
  #tercero{
    text-decoration:line-through;
  }
  .content {
    padding: 1% 3%;
    /* margin: 0 -4%; */
  }
  .list-one {
    font-size: 12px;
  }
  .p-one {
    font-size: 12px;
  }
  .tac{
    text-align: center;
  }
  .card {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid rgba(0, 0, 0, 0.125);
    border-radius: 0.25rem;
  }

  .card > hr {
    margin-right: 0;
    margin-left: 0;
  }

  .card > .list-group:first-child .list-group-item:first-child {
    border-top-left-radius: 0.25rem;
    border-top-right-radius: 0.25rem;
  }

  .card > .list-group:last-child .list-group-item:last-child {
    border-bottom-right-radius: 0.25rem;
    border-bottom-left-radius: 0.25rem;
  }

  .card-body {
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    padding: 1.25rem;
  }

  .card-header {
    padding: 0.75rem 1.25rem;
    margin-bottom: 0;
    background-color: rgba(0, 0, 0, 0.03);
    border-bottom: 1px solid rgba(0, 0, 0, 0.125);
  }

  .card-header:first-child {
    border-radius: calc(0.25rem - 1px) calc(0.25rem - 1px) 0 0;
  }

  .card-header + .list-group .list-group-item:first-child {
    border-top: 0;
  }


  hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid rgba(0, 0, 0, 0.1);
  }

  h1, h2, h3, h4, h5, h6 {
    margin-top: 0;
    margin-bottom: 0.5rem;
  }

  h6, .h6 {
    font-size: 1rem;
  }

  p {
    margin-top: 0;
    margin-bottom: 1rem;
  }

  .badge {
    display: inline-block;
    padding: 0.25em 0.4em;
    font-size: 75%;
    font-weight: 700;
    line-height: 1;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0.25rem;
    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  }

  .badge {
    border: 1px solid #17a2b8;
  }

  .badge-info {
    color: #fff;
    background-color: #17a2b8;
  }

  .badge-secondary {
    color: #fff;
    background-color: #6c757d;
    border: 1px solid #6c757d;
  }

  a.badge-secondary:hover, a.badge-secondary:focus {
    color: #fff;
    background-color: #545b62;
  }

  a.badge-secondary:focus, a.badge-secondary.focus {
    outline: 0;
    box-shadow: 0 0 0 0.2rem rgba(108, 117, 125, 0.5);
  }




  </style>
</head>
<body>
  <div class="content">
    <div class="card" style="page-break-after: always;top:50%;">
      <div class="card-body">
        <h1> <b> Reporte Detallado de {{$state}}</b></h1>
      </div>
    </div>

    <div class="table-responsive" >
      <table class="table" >
        <thead>
          <tr>
            <th style="text-align:center;background:#13385d !important;color:white;" width="100%"> <h6>Mapa Del Estado</h6> </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <p class="p-one">Mapa del estado que incluye:</p>
              <ul class="list-one">
                <li>Sedes de la Fiscalía/Procuraduría</li>
                <li>Municipios dentro de las sedes de la Fiscalía/Procuraduría</li>
                <li>Número de Agentes del Ministerio Público por sede</li>
              </ul>
              <div class="tac">
                <img src="{{$map}}" height="300px">
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th width="10%" class="p-one">SEDE DE LA FISCALÍA / PROCURADURÍA</th>
                    <th width="60%" class="p-one">MUNICIPIOS POR SEDE	</th>
                    <th width="30%" class="p-one">NÚMERO DE AGENTES DEL MINISTERIO PÚBLICO POR SEDE</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($allheadquarters as $key_ah => $value_ah)
                  <tr>
                    <td class="p-one">{{$value_ah->name}}</td>
                    <td class="p-one">{{$value_ah->city_halls}}</td>
                    <td class="p-one">{{$value_ah->agents}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div style="page-break-after: always;">
      <br><br><br>
    </div>
    <div class="table-responsive" >
      <table class="table" >
        <thead>
          <tr>
            <th style="text-align:center;background:#13385d !important;color:white;" width="100%"> <h6>Estructura Organizacional</h6> </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <p class="p-one"> <b>ORGANIGRAMA</b> </p>
              <p class="p-one">Organigrama de la institución estructurado a nivel de detalle que permite conocer la composición de las unidades de investigación.</p>

            </td>
          </tr>
          @foreach ($orgdata as $key_od => $value_od)
          <tr>
            <td>
              <div class="tac">
                <img src="{{'myFiles/'.$value_od->path}}" height="320px"><br><br>
              </div>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <br><br>
    <br>

    @foreach ($desisions as $keyd => $valued)
    <div class="card"  style="page-break-inside: avoid;">
      <div class="card-header" style="background:#13385d;color:white;">
        Desición {{$valued->id}}: {{$valued->name}}
      </div>
      <div class="card-body">
        <span class="badge badge-info" >Ámbito Regulatorio</span><br>
        @foreach ($conclusion as $keyc => $valuec)
        @if($valuec['data']->decision_id == $valued->id && $valuec['data']->ambit_id == 1)
        @foreach ($valuec['child'] as $keych => $valuech)
        <div style="font-size:14px;"  style="page-break-inside: avoid;">
        <b>Conclusión</b>
          {!! preg_replace("[\n|\r|\n\r|<br>]", "", $valuech->conclusion_ambit) !!}
        </div>
        <div style="font-size:14px;"  style="page-break-inside: avoid;">
        <b>Problemática</b>
        {!! preg_replace("[\n|\r|\n\r|<br>]", "", $valuech->problematic_ambit) !!}
        </div>
        <hr>
        @endforeach
        @endif
        @endforeach



        <span class="badge badge-info">Ámbito Institucional</span><br>
        @foreach ($conclusion as $keyc => $valuec)
        @if($valuec['data']->decision_id == $valued->id && $valuec['data']->ambit_id == 2)
        @foreach ($valuec['child'] as $keych => $valuech)
        <div style="font-size:14px;"  style="page-break-inside: avoid;">
        <b>Conclusión</b>
          {!! preg_replace("[\n|\r|\n\r|<br>]", "", $valuech->conclusion_ambit) !!}
        </div>
        <div style="font-size:14px;"  style="page-break-inside: avoid;">
        <b>Problemática</b>
        {!! preg_replace("[\n|\r|\n\r|<br>]", "", $valuech->problematic_ambit) !!}
        </div>
        <hr>
        @endforeach
        @endif
        @endforeach

        <span class="badge badge-info">Ámbito Individual</span><br>
        @foreach ($conclusion as $keyc => $valuec)
        @if($valuec['data']->decision_id == $valued->id && $valuec['data']->ambit_id == 3)
        @foreach ($valuec['child'] as $keych => $valuech)
        <div style="font-size:14px;"  style="page-break-inside: avoid;">
        <b>Conclusión</b>
          {!! preg_replace("[\n|\r|\n\r|<br>]", "", $valuech->conclusion_ambit) !!}
        </div>
        <div style="font-size:14px;"  style="page-break-inside: avoid;">
        <b>Problemática</b>
        {!! preg_replace("[\n|\r|\n\r|<br>]", "", $valuech->problematic_ambit) !!}
        </div>
        <hr>
        @endforeach
        @endif
        @endforeach

        @foreach ($years as $keyy => $valuey)
        <table class="table table-bordered">
          <tr>
            <th style="font-size:11px;">INDICADOR <span class="badge badge-secondary">{{$valuey}}</span></th>
            <th style="font-size:11px;">PORCENTAJE</th>
            <th style="font-size:11px;">INTERPRETACIÓN</th>
          </tr>
          @foreach ($indicators as $keyi => $valuei)
          @if ($valuei->decision_id == $valued->id)
          {{$textdiviver = 'variable'.$valuei->dividerreal.'_'.$valuey}}
          {{$textdividend = 'variable'.$valuei->dividendreal.'_'.$valuey}}
          <tr>
            <td style="font-size:10px;">{{$valuei->name}}</td>
            <td style="font-size:10px;text-align:center;">
              @if (array_key_exists($textdiviver, $quantitativesources) && array_key_exists($textdividend, $quantitativesources))

              @if ($quantitativesources[$textdiviver] == null || $quantitativesources[$textdividend] == null)
              N.D.
              @else
              {{ number_format((($quantitativesources[$textdividend] / $quantitativesources[$textdiviver]) * 100),1,'.','') .'%'}}
              @endif


              @endif
            </td>
            <td style="font-size:10px;text-align:justify;">
              @if(array_key_exists('interpretation'.$valuei->id.'_'.$valuey,$interpretations))
              {!! $interpretations['interpretation'.$valuei->id.'_'.$valuey] !!}
              @endif
            </td>
          </tr>
          @endif
          @endforeach
        </table>
        <br>
        @endforeach


      </div>
    </div>
    <br>


    @endforeach





  </div>
</body>
</html>
