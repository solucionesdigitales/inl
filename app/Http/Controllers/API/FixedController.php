<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Observation;
use App\Subsection;
use App\AllHeadquarters;
use App\Headquarter;
use App\Interpretation;
use App\CriterionConclusions;
use App\DataLoaded;
use App\State;
use App\Conclusion;
use App\ConclusionProblematicAmbit;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

class FixedController extends Controller
{
  //
  public function contar($id)
  {
    // code...
    $state_observation = Observation::where('state_id',$id)->get();
    return response()->json(count($state_observation));
  }

  public function revisar($id)
  {
    $state_observation = Observation::where('state_id',$id)->get();
    $arr = [];
    foreach ($state_observation as $key => $value) {
      $state_observation = Observation::where('state_id',$id)
      ->where('decision_id',$value->decision_id)
      ->where('ambit_id',$value->ambit_id)
      ->where('criterion_id',$value->criterion_id)
      ->where('source_id',$value->source_id)
      ->get();
      if (count($state_observation) > 1) {
        $arr [] = $state_observation;
      }
    }

    return response()->json(($arr));
  }

  public function eliminar($id)
  {
    $state_observation = Observation::where('state_id',$id)->get();
    foreach ($state_observation as $key => $value) {
      $state_observation = Observation::where('state_id',$id)
      ->where('decision_id',$value->decision_id)
      ->where('ambit_id',$value->ambit_id)
      ->where('criterion_id',$value->criterion_id)
      ->where('source_id',$value->source_id)
      ->get();
      if (count($state_observation) > 1) {
        $state_observation_delete = Observation::where('id',$state_observation[0]['id'])->delete();
      }
    }
    return response()->json(['status' => true]);
  }

  public function fixed($id)
  {
    try {

      DB::beginTransaction();
      $state_observation = Observation::where('state_id',$id)->get();
      $headquarter = Headquarter::where('state_id',$id)->first();
      $headquarter_id = 0;

      if (isset($headquarter) == false) {

        $all_headquarters = AllHeadquarters::where('state_id',$id)->first();
        if (isset($all_headquarters) == false) {
          $all_headquarters_new = new AllHeadquarters();
          $all_headquarters_new->state_id = $id;
          $all_headquarters_new->name = 'Pendiente';
          $all_headquarters_new->city_halls = 'Pendiente';
          $all_headquarters_new->agents = 0;
          $all_headquarters_new->save();

          $headquarter_new = new Headquarter();
          $headquarter_new->state_id = $id;
          $headquarter_new->name = 'Pendiente';
          $headquarter_new->city_halls = 'Pendiente';
          $headquarter_new->agents = 0;
          $headquarter_new->save();

          $headquarter_id = $headquarter_new->id;

        }else {
          $headquarter_new = new Headquarter();
          $headquarter_new->state_id = $id;
          $headquarter_new->name = $all_headquarters->name;
          $headquarter_new->city_halls = $all_headquarters->city_halls;
          $headquarter_new->agents = $all_headquarters->agents;
          $headquarter_new->save();

          $headquarter_id = $headquarter_new->id;
        }
      }else {
        $headquarter_id = $headquarter->id;
      }

      $subsections = Subsection::get();
      $arreglo = [];
      foreach ($subsections as $key => $value) {
        $state_observation_f = Observation::where('state_id',$id)
        ->where('decision_id',$value->decision_id)
        ->where('ambit_id',$value->ambit_id)
        ->where('criterion_id',$value->criterion_id)
        ->where('source_id',$value->source_id)
        ->get();

        if (count($state_observation_f) != 0) {
          $arreglo [] = $state_observation_f;
        }else {
          //se debe agregar el registro
          $arreglo [] = ['vacio', $value];
          $state_observation_new = new Observation();
          $state_observation_new->state_id = $id;
          $state_observation_new->headquarter_id = $headquarter_id;
          $state_observation_new->decision_id = $value->decision_id;
          $state_observation_new->ambit_id = $value->ambit_id;
          $state_observation_new->criterion_id = $value->criterion_id;
          $state_observation_new->source_id = $value->source_id;
          // $state_observation_new->observation = '';
          $state_observation_new->save();
        }

      }
      DB::commit();
      return response()->json($arreglo);
    } catch (\Exception $e) {
      DB::rollback();
      dd($e);
    }
  }

  public function fixedIndicators($id)
  {
    try {
      DB::beginTransaction();
      $intepretation = Interpretation::
      select('id',
          'state_id',
          'interpretation1',
          'interpretation2',
          'interpretation3',
          'interpretation4',
          'interpretation5',
          'interpretation6',
          'interpretation7',
          'interpretation8',
          'interpretation9',
          'interpretation10',
          'interpretation11',
          'interpretation12',
          'interpretation13',
          'interpretation14',
          'interpretation15',
          'interpretation16',
          'interpretation17',
          'interpretation18',
          'interpretation19',
          'interpretation20',
          'interpretation21',
          'interpretation22',
          'interpretation23',
          'interpretation24',
          'interpretation25',
          'interpretation26',
          'interpretation27',
          'interpretation28',
          'interpretation29',
          'interpretation30',
          'interpretation31',
          'interpretation32',
          'interpretation33',
          'interpretation34',
          'interpretation35',
          'interpretation36',
          'created_at',
          'updated_at'
          )
      ->where('state_id',$id)->first();

      $data = [
        'id' => $intepretation->id,
        'state_id' => $intepretation->state_id,
        'interpretation1_2018' => $intepretation->interpretation1,
        'interpretation2_2018' => $intepretation->interpretation2,
        'interpretation3_2018' => $intepretation->interpretation3,
        'interpretation4_2018' => $intepretation->interpretation4,
        'interpretation5_2018' => $intepretation->interpretation5,
        'interpretation6_2018' => $intepretation->interpretation6,
        'interpretation7_2018' => $intepretation->interpretation7,
        'interpretation8_2018' => $intepretation->interpretation8,
        'interpretation9_2018' => $intepretation->interpretation9,
        'interpretation10_2018' => $intepretation->interpretation10,
        'interpretation11_2018' => $intepretation->interpretation11,
        'interpretation12_2018' => $intepretation->interpretation12,
        'interpretation13_2018' => $intepretation->interpretation13,
        'interpretation14_2018' => $intepretation->interpretation14,
        'interpretation15_2018' => $intepretation->interpretation15,
        'interpretation16_2018' => $intepretation->interpretation16,
        'interpretation17_2018' => $intepretation->interpretation17,
        'interpretation18_2018' => $intepretation->interpretation18,
        'interpretation19_2018' => $intepretation->interpretation19,
        'interpretation20_2018' => $intepretation->interpretation20,
        'interpretation21_2018' => $intepretation->interpretation21,
        'interpretation22_2018' => $intepretation->interpretation22,
        'interpretation23_2018' => $intepretation->interpretation23,
        'interpretation24_2018' => $intepretation->interpretation24,
        'interpretation25_2018' => $intepretation->interpretation25,
        'interpretation26_2018' => $intepretation->interpretation26,
        'interpretation27_2018' => $intepretation->interpretation27,
        'interpretation28_2018' => $intepretation->interpretation28,
        'interpretation29_2018' => $intepretation->interpretation29,
        'interpretation30_2018' => $intepretation->interpretation30,
        'interpretation31_2018' => $intepretation->interpretation31,
        'interpretation32_2018' => $intepretation->interpretation32,
        'interpretation33_2018' => $intepretation->interpretation33,
        'interpretation34_2018' => $intepretation->interpretation34,
        'interpretation35_2018' => $intepretation->interpretation35,
        'interpretation36_2018' => $intepretation->interpretation36,

        'interpretation1_2017' => '',
        'interpretation2_2017' => '',
        'interpretation3_2017' => '',
        'interpretation4_2017' => '',
        'interpretation5_2017' => '',
        'interpretation6_2017' => '',
        'interpretation7_2017' => '',
        'interpretation8_2017' => '',
        'interpretation9_2017' => '',
        'interpretation10_2017' => '',
        'interpretation11_2017' => '',
        'interpretation12_2017' => '',
        'interpretation13_2017' => '',
        'interpretation14_2017' => '',
        'interpretation15_2017' => '',
        'interpretation16_2017' => '',
        'interpretation17_2017' => '',
        'interpretation18_2017' => '',
        'interpretation19_2017' => '',
        'interpretation20_2017' => '',
        'interpretation21_2017' => '',
        'interpretation22_2017' => '',
        'interpretation23_2017' => '',
        'interpretation24_2017' => '',
        'interpretation25_2017' => '',
        'interpretation26_2017' => '',
        'interpretation27_2017' => '',
        'interpretation28_2017' => '',
        'interpretation29_2017' => '',
        'interpretation30_2017' => '',
        'interpretation31_2017' => '',
        'interpretation32_2017' => '',
        'interpretation33_2017' => '',
        'interpretation34_2017' => '',
        'interpretation35_2017' => '',
        'interpretation36_2017' => '',

        'interpretation1_2019' => '',
        'interpretation2_2019' => '',
        'interpretation3_2019' => '',
        'interpretation4_2019' => '',
        'interpretation5_2019' => '',
        'interpretation6_2019' => '',
        'interpretation7_2019' => '',
        'interpretation8_2019' => '',
        'interpretation9_2019' => '',
        'interpretation10_2019' => '',
        'interpretation11_2019' => '',
        'interpretation12_2019' => '',
        'interpretation13_2019' => '',
        'interpretation14_2019' => '',
        'interpretation15_2019' => '',
        'interpretation16_2019' => '',
        'interpretation17_2019' => '',
        'interpretation18_2019' => '',
        'interpretation19_2019' => '',
        'interpretation20_2019' => '',
        'interpretation21_2019' => '',
        'interpretation22_2019' => '',
        'interpretation23_2019' => '',
        'interpretation24_2019' => '',
        'interpretation25_2019' => '',
        'interpretation26_2019' => '',
        'interpretation27_2019' => '',
        'interpretation28_2019' => '',
        'interpretation29_2019' => '',
        'interpretation30_2019' => '',
        'interpretation31_2019' => '',
        'interpretation32_2019' => '',
        'interpretation33_2019' => '',
        'interpretation34_2019' => '',
        'interpretation35_2019' => '',
        'interpretation36_2019' => '',

        'interpretation1_2020' => '',
        'interpretation2_2020' => '',
        'interpretation3_2020' => '',
        'interpretation4_2020' => '',
        'interpretation5_2020' => '',
        'interpretation6_2020' => '',
        'interpretation7_2020' => '',
        'interpretation8_2020' => '',
        'interpretation9_2020' => '',
        'interpretation10_2020' => '',
        'interpretation11_2020' => '',
        'interpretation12_2020' => '',
        'interpretation13_2020' => '',
        'interpretation14_2020' => '',
        'interpretation15_2020' => '',
        'interpretation16_2020' => '',
        'interpretation17_2020' => '',
        'interpretation18_2020' => '',
        'interpretation19_2020' => '',
        'interpretation20_2020' => '',
        'interpretation21_2020' => '',
        'interpretation22_2020' => '',
        'interpretation23_2020' => '',
        'interpretation24_2020' => '',
        'interpretation25_2020' => '',
        'interpretation26_2020' => '',
        'interpretation27_2020' => '',
        'interpretation28_2020' => '',
        'interpretation29_2020' => '',
        'interpretation30_2020' => '',
        'interpretation31_2020' => '',
        'interpretation32_2020' => '',
        'interpretation33_2020' => '',
        'interpretation34_2020' => '',
        'interpretation35_2020' => '',
        'interpretation36_2020' => '',

        'created_at' => $intepretation->created_at,
        'updated_at' => $intepretation->updated_at,
      ];

      $intepretation->data =json_encode($data);
      $intepretation->save();
      DB::commit();
      return response()->json(($intepretation));
    } catch (\Exception $e) {
      DB::rollback();
      dd($e);
    }

  }

  public function fixedIndicatorsRoundTwo($id)
  {
    try {
      DB::beginTransaction();

      $data = [
        'id' => $id,//$id
        'state_id' => $id,
        'interpretation37_2018' => '',
        'interpretation38_2018' => '',
        'interpretation39_2018' => '',
        'interpretation40_2018' => '',
        'interpretation41_2018' => '',
        'interpretation42_2018' => '',
        'interpretation43_2018' => '',
        'interpretation44_2018' => '',
        'interpretation45_2018' => '',
        'interpretation46_2018' => '',
        'interpretation47_2018' => '',
        'interpretation48_2018' => '',
        'interpretation49_2018' => '',
        'interpretation50_2018' => '',
        'interpretation51_2018' => '',
        'interpretation52_2018' => '',
        'interpretation53_2018' => '',
        'interpretation54_2018' => '',
        'interpretation55_2018' => '',
        'interpretation56_2018' => '',
        'interpretation57_2018' => '',
        'interpretation58_2018' => '',
        'interpretation59_2018' => '',
        'interpretation60_2018' => '',
        'interpretation61_2018' => '',
        'interpretation62_2018' => '',
        'interpretation63_2018' => '',
        'interpretation64_2018' => '',
        'interpretation65_2018' => '',
        'interpretation66_2018' => '',
        'interpretation67_2018' => '',
        'interpretation68_2018' => '',
        'interpretation69_2018' => '',
        'interpretation70_2018' => '',
        'interpretation71_2018' => '',
        'interpretation72_2018' => '',
        'interpretation73_2018' => '',
        'interpretation74_2018' => '',
        'interpretation75_2018' => '',

        'interpretation37_2017' => '',
        'interpretation38_2017' => '',
        'interpretation39_2017' => '',
        'interpretation40_2017' => '',
        'interpretation41_2017' => '',
        'interpretation42_2017' => '',
        'interpretation43_2017' => '',
        'interpretation44_2017' => '',
        'interpretation45_2017' => '',
        'interpretation46_2017' => '',
        'interpretation47_2017' => '',
        'interpretation48_2017' => '',
        'interpretation49_2017' => '',
        'interpretation50_2017' => '',
        'interpretation51_2017' => '',
        'interpretation52_2017' => '',
        'interpretation53_2017' => '',
        'interpretation54_2017' => '',
        'interpretation55_2017' => '',
        'interpretation56_2017' => '',
        'interpretation57_2017' => '',
        'interpretation58_2017' => '',
        'interpretation59_2017' => '',
        'interpretation60_2017' => '',
        'interpretation61_2017' => '',
        'interpretation62_2017' => '',
        'interpretation63_2017' => '',
        'interpretation64_2017' => '',
        'interpretation65_2017' => '',
        'interpretation66_2017' => '',
        'interpretation67_2017' => '',
        'interpretation68_2017' => '',
        'interpretation69_2017' => '',
        'interpretation70_2017' => '',
        'interpretation71_2017' => '',
        'interpretation72_2017' => '',
        'interpretation73_2017' => '',
        'interpretation74_2017' => '',
        'interpretation75_2017' => '',

        'interpretation37_2019' => '',
        'interpretation38_2019' => '',
        'interpretation39_2019' => '',
        'interpretation40_2019' => '',
        'interpretation41_2019' => '',
        'interpretation42_2019' => '',
        'interpretation43_2019' => '',
        'interpretation44_2019' => '',
        'interpretation45_2019' => '',
        'interpretation46_2019' => '',
        'interpretation47_2019' => '',
        'interpretation48_2019' => '',
        'interpretation49_2019' => '',
        'interpretation50_2019' => '',
        'interpretation51_2019' => '',
        'interpretation52_2019' => '',
        'interpretation53_2019' => '',
        'interpretation54_2019' => '',
        'interpretation55_2019' => '',
        'interpretation56_2019' => '',
        'interpretation57_2019' => '',
        'interpretation58_2019' => '',
        'interpretation59_2019' => '',
        'interpretation60_2019' => '',
        'interpretation61_2019' => '',
        'interpretation62_2019' => '',
        'interpretation63_2019' => '',
        'interpretation64_2019' => '',
        'interpretation65_2019' => '',
        'interpretation66_2019' => '',
        'interpretation67_2019' => '',
        'interpretation68_2019' => '',
        'interpretation69_2019' => '',
        'interpretation70_2019' => '',
        'interpretation71_2019' => '',
        'interpretation72_2019' => '',
        'interpretation73_2019' => '',
        'interpretation74_2019' => '',
        'interpretation75_2019' => '',

        'interpretation37_2020' => '',
        'interpretation38_2020' => '',
        'interpretation39_2020' => '',
        'interpretation40_2020' => '',
        'interpretation41_2020' => '',
        'interpretation42_2020' => '',
        'interpretation43_2020' => '',
        'interpretation44_2020' => '',
        'interpretation45_2020' => '',
        'interpretation46_2020' => '',
        'interpretation47_2020' => '',
        'interpretation48_2020' => '',
        'interpretation49_2020' => '',
        'interpretation50_2020' => '',
        'interpretation51_2020' => '',
        'interpretation52_2020' => '',
        'interpretation53_2020' => '',
        'interpretation54_2020' => '',
        'interpretation55_2020' => '',
        'interpretation56_2020' => '',
        'interpretation57_2020' => '',
        'interpretation58_2020' => '',
        'interpretation59_2020' => '',
        'interpretation60_2020' => '',
        'interpretation61_2020' => '',
        'interpretation62_2020' => '',
        'interpretation63_2020' => '',
        'interpretation64_2020' => '',
        'interpretation65_2020' => '',
        'interpretation66_2020' => '',
        'interpretation67_2020' => '',
        'interpretation68_2020' => '',
        'interpretation69_2020' => '',
        'interpretation70_2020' => '',
        'interpretation71_2020' => '',
        'interpretation72_2020' => '',
        'interpretation73_2020' => '',
        'interpretation74_2020' => '',
        'interpretation75_2020' => ''
      ];

      $intepretation_find = Interpretation::where('state_id',$id)->first();

      if (isset($intepretation_find) == false) {
        $intepretation_new = new Interpretation();
        $intepretation_new->state_id = $id;
        $intepretation_new->data =json_encode($data);
        $intepretation_new->save();
      }else {
        $intepretation_find->state_id = $id;
        $intepretation_find->data =json_encode($data);
        $intepretation_find->save();
      }

      DB::commit();
      return response()->json(['status' => true]);
    } catch (\Exception $e) {
      DB::rollback();
      dd($e);
    }

  }

  public function CriterionConclusionsRoundTwo()
  {
    try {
      DB::beginTransaction();
      $subsections = Subsection::where('round_two','1')
      ->select('decision_id','ambit_id','criterion_id')
      ->groupBy('decision_id','ambit_id','criterion_id')
      ->get();

      for ($i=37; $i <= 67 ; $i++) {
        foreach ($subsections as $key => $value) {
          $cc = new CriterionConclusions();
          $cc->ambit_id = $value->ambit_id;
          $cc->decision_id = $value->decision_id;
          $cc->criterion_id = $value->criterion_id;
          $cc->state_id = $i;
          $cc->save();
        }
      }
      DB::commit();
      return response()->json(['status' => true]);
    } catch (\Exception $e) {
      DB::rollback();
      dd($e);
    }


  }

  public function saveLoadedDataOne()
  {
    try {
      DB::beginTransaction();
      // DataLoaded
      // NOTE: 1.- MAP, 2.- ORG, 3.-Headquarter.

      $data_delete = DataLoaded::whereIn('type',['map','org','head'])->delete();

      $states = State::get();

      foreach ($states as $key_s => $value_s) {
        $per_map = 0; $per_org = 0; $per_hea = 0;
        $map = DB::table('documents')->where('type','LIKE','map')->where('state_id',$value_s->id)->count();
        $organization = DB::table('documents')->where('type','LIKE','organization')->where('state_id',$value_s->id)->count();
        $headquarter = DB::table('all_headquarters')->where('state_id',$value_s->id)->count();

        if ($map > 0) {
          $per_map = 100;
        }
        $dataLoadedm = new DataLoaded();
        $dataLoadedm->state_id = $value_s->id;
        $dataLoadedm->percentage = $per_map;
        $dataLoadedm->total_amount = 1;
        $dataLoadedm->registered_amount = $map;
        $dataLoadedm->type = 'map';
        $dataLoadedm->save();

        if ($organization > 0) {
          $per_org = 100;
        }
        $dataLoadedo = new DataLoaded();
        $dataLoadedo->state_id = $value_s->id;
        $dataLoadedo->percentage = $per_org;
        $dataLoadedo->total_amount = 1;
        $dataLoadedo->registered_amount = $organization;
        $dataLoadedo->type = 'org';
        $dataLoadedo->save();

        if ($headquarter > 0) {
          $per_hea = 100;
        }
        $dataLoadedh = new DataLoaded();
        $dataLoadedh->state_id = $value_s->id;
        $dataLoadedh->percentage = $per_hea;
        $dataLoadedh->total_amount = 1;
        $dataLoadedh->registered_amount = $headquarter;
        $dataLoadedh->type = 'head';
        $dataLoadedh->save();
      }

      DB::commit();
      return response()->json(['status' => true]);
    } catch (\Exception $e) {
      DB::rollback();
      dd($e);
    }
  }

  public function saveLoadedDataTwo()
  {
    try {
      $data_delete = DataLoaded::where('type','cuanti')->delete();

        $arreglo = [];
      $states = State::get();
      foreach ($states as $key => $value_s) {
        $qs = DB::table('quantitative_sources')->where('state_id',$value_s->id)->first();
        $data_c_2017 = 0;
        $data_c_2018 = 0;
        $json = json_decode($qs->data);
        foreach ($json as $key => $value) {
          for ($i=1; $i < 36 ; $i++) {
            if ($key == 'checked'.$i.'_2017' && $value != 0) {
              $data_c_2017 += 1;
            }
            if ($key == 'variable'.$i.'_2017' && !is_null($value)) {
              $data_c_2017 += 1;
            }
            if ($key == 'checked'.$i.'_2018' && $value != 0) {
              $data_c_2018 += 1;
            }
            if ($key == 'variable'.$i.'_2018' && !is_null($value)) {
              $data_c_2018 += 1;
            }

          }
        }
        //
        $vd = 0;
        if ($value_s->round == 1) {
          $vd = 33 * 2;
        }elseif ($value_s->round == 2) {
          $vd = 43 * 4;
        }

        $progress = (( ($data_c_2017 + $data_c_2018) * 100) / $vd);

        $dataLoadedh = new DataLoaded();
        $dataLoadedh->state_id = $value_s->id;
        $dataLoadedh->percentage = $progress;
        $dataLoadedh->total_amount = $vd;
        $dataLoadedh->registered_amount = ($data_c_2017 + $data_c_2018);
        $dataLoadedh->type = 'cuanti';
        $dataLoadedh->save();

      }
        return response()->json(['status' => true]);
    } catch (\Exception $e) {
      DB::rollback();
      dd($e);
    }
  }

  public function saveLoadedDataThree()
  {
    try {
      $data_delete = DataLoaded::whereIn('type',['conclusions','problematics','observations','criterion_conclusion'])->delete();

      $states = State::get();

      foreach ($states as $key_state => $value_state) {

        $t_con = 39;
        $t_pro = 39;

        $conclusion = DB::table('conclusions')->where('state_id',$value_state->id)->first();
        $a = 0;
        foreach ($conclusion as $key => $value) {
          for ($i=1; $i < 4 ; $i++) {
            for ($j=1; $j < 14 ; $j++) {
              if ($key == 'conclusion-'.$j.'-'.$i) {
                if ($value != '' || !is_null($value)) {
                  $a += 1;
                }
              }
            }
          }
        }
        $porcertage_conclusions = round(($a/$t_con) * 100);//conclusions

        $dataLoadec = new DataLoaded();
        $dataLoadec->state_id = $value_state->id;
        $dataLoadec->percentage = $porcertage_conclusions;
        $dataLoadec->total_amount = $t_con;
        $dataLoadec->registered_amount = $a;
        $dataLoadec->type = 'conclusions';
        $dataLoadec->save();

        $problematic = DB::table('problematics')->where('state_id',$value_state->id)->first();
        $a_p = 0;
        foreach ($problematic as $key_p => $value_p) {
          for ($k=1; $k < 4 ; $k++) {
            for ($l=1; $l < 14 ; $l++) {
              if ($key_p == 'problematic-'.$l.'-'.$k) {
                if ($value_p != '' || !is_null($value_p)) {
                  $a_p += 1;
                }
              }
            }
          }
        }

        $porcertage_problematics = round(($a_p/$t_pro) * 100);//problematics

        $dataLoadec = new DataLoaded();
        $dataLoadec->state_id = $value_state->id;
        $dataLoadec->percentage = $porcertage_problematics;
        $dataLoadec->total_amount = $t_pro;
        $dataLoadec->registered_amount = $a_p;
        $dataLoadec->type = 'problematics';
        $dataLoadec->save();

        $observation = DB::table('observations')->where('state_id',$value_state->id)->get();
        $t_obs = count($observation);
        $a_o = 0;
        foreach ($observation as $key => $value) {
          if ($value->observation != '' ||!is_null($value->observation)) {
            $a_o += 1;
          }
        }

        $porcertage_observations = round(($a_o/$t_obs) * 100);//observations

        $dataLoadec = new DataLoaded();
        $dataLoadec->state_id = $value_state->id;
        $dataLoadec->percentage = $porcertage_observations;
        $dataLoadec->total_amount = $t_obs;
        $dataLoadec->registered_amount = $a_o;
        $dataLoadec->type = 'observations';
        $dataLoadec->save();

        $criterion_conclusion = DB::table('criterion_conclusions')->where('state_id',$value_state->id)->get();
        if (count($criterion_conclusion) > 0) {
          $t_cc = 0;
          // if ($value_state->round == 2) {
          //   $t_cc = count($criterion_conclusion) * 3;
          // }else {
            $t_cc = count($criterion_conclusion);
          // }
          $a_cc = 0;

          foreach ($criterion_conclusion as $key_cc => $value_cc) {
            if ($value_cc->conclusion != '' || !is_null($value_cc->conclusion)) {
              $a_cc += 1;
            }
            // if ($value_state->round == 2) {
            //   if ($value_cc->conclusion_problematica != '' || !is_null($value_cc->conclusion_problematica)) {
            //     $a_cc += 1;
            //   }
            //   if ($value_cc->problematica_conclusion != '' || !is_null($value_cc->problematica_conclusion)) {
            //     $a_cc += 1;
            //   }
            // }
          }
          $porcertage_criterion_conclusion = round(($a_cc/$t_cc) * 100);//criterion_conclusion

          $dataLoadec = new DataLoaded();
          $dataLoadec->state_id = $value_state->id;
          $dataLoadec->percentage = $porcertage_criterion_conclusion;
          $dataLoadec->total_amount = $t_cc;
          $dataLoadec->registered_amount = $a_cc;
          $dataLoadec->type = 'criterion_conclusion';
          $dataLoadec->save();
        }else {
          $dataLoadec = new DataLoaded();
          $dataLoadec->state_id = $value_state->id;
          $dataLoadec->percentage = 0;
          $dataLoadec->total_amount = 167;
          $dataLoadec->registered_amount = 0;
          $dataLoadec->type = 'criterion_conclusion';
          $dataLoadec->save();
        }

      }

      return response()->json(['status' => true]);

    } catch (\Exception $e) {
      DB::rollback();
      dd($e);
    }
  }

  public function fixedcriterionconcusion()
  {
    // use App\CriterionConclusions;
    $cc = CriterionConclusions::where('state_id','>','36')->where('conclusion_problematica','!=','')->get();
    foreach ($cc as $key => $value) {
      $cd = Conclusion::updateOrCreate(
        ['state_id' => $value->state_id],
        ['conclusion-'.$value->decision_id.'-'.$value->ambit_id => $value->conclusion_problematica]
      );
      // ->where('id',$value->id);
        // $cc_save = Conclusion::where('id',$value->id)->first();
      // $cc_save->conclusion = $value->conclusion_problematica;
      // $cc_save->save();
    }

    return response()->json($cc);
  }

  public function convertJsonCriterionConclusion()
  {
    $cc = CriterionConclusions::whereNotNull('conclusion_ambit')->orWhereNotNull('problematic_ambit')->get();
    foreach ($cc as $key => $value) {
      $cpa = new ConclusionProblematicAmbit();
      $cpa->criterion_conclusions_id = $value->id;
      $cpa->conclusion_ambit = $value->conclusion_ambit;
      $cpa->problematic_ambit = $value->problematic_ambit;
      $cpa->save();

      $cc_r = CriterionConclusions::where('id',$value->id)->first();
      $cc_r->conclusion_ambit = '-';
      $cc_r->problematic_ambit = '-';
      $cc_r->save();

    }

    return response()->json($cc);
  }

  public function getInterData($id)
  {

    $json_v = '[
 {
  "num": 1,
  "name": " Total de carpetas de investigación.",
  "id_round1": 1,
  "id_round2": 34,
  "id_round3": 77
 },
 {
  "num": 1.1,
  "name": "Total de carpetas de investigación iniciadas por un informe policial",
  "id_round1": 0,
  "id_round2": 35,
  "id_round3": 78
 },
 {
  "num": "1.1.1",
  "name": "Total de carpetas de investigación iniciadas por un informe policial sin detenido",
  "id_round1": 2,
  "id_round2": 36,
  "id_round3": 79
 },
 {
  "num": "1.1.2",
  "name": "Total de carpetas de investigación iniciadas por un informe policial con detenido",
  "id_round1": 3,
  "id_round2": 37,
  "id_round3": 80
 },
 {
  "num": 1.2,
  "name": "Total de carpetas de investigación iniciadas por una denuncia o querella de víctima u ofendido ante el Agente del Ministerio Público",
  "id_round1": 4,
  "id_round2": 38,
  "id_round3": 81
 },
 {
  "num": 1.3,
  "name": " Total de carpetas de investigación iniciadas por otra fuente",
  "id_round1": 5,
  "id_round2": 39,
  "id_round3": 82
 },
 {
  "num": 2,
  "name": "Total de detenidos",
  "id_round1": 6,
  "id_round2": 40,
  "id_round3": 83
 },
 {
  "num": 2.1,
  "name": "Total de personas puestas a disposición ratificadas por un Agente del Ministerio Público",
  "id_round1": 7,
  "id_round2": 41,
  "id_round3": 84
 },
 {
  "num": 2.2,
  "name": "Total de personas puestas a disposición que no fueron ratificadas por un Agente del Ministerio Público",
  "id_round1": 0,
  "id_round2": 42,
  "id_round3": 85
 },
 {
  "num": 3,
  "name": "Total de personas detenidas con audiencias de control de detención celebradas",
  "id_round1": 8,
  "id_round2": 43,
  "id_round3": 86
 },
 {
  "num": 3.1,
  "name": "Total de personas detenidas en cuyas audiencias de control se dictó legal la detención",
  "id_round1": 9,
  "id_round2": 44,
  "id_round3": 87
 },
 {
  "num": 3.2,
  "name": "Total de personas detenidas en cuyas audiencias de control se dictó ilegal la detención",
  "id_round1": 0,
  "id_round2": 45,
  "id_round3": 88
 },
 {
  "num": 4,
  "name": " Total de carpetas de investigación determinadas mediante facultades para la terminación de la investigación",
  "id_round1": 10,
  "id_round2": 46,
  "id_round3": 89
 },
 {
  "num": 4.1,
  "name": "Total de carpetas de investigación determinadas en archivo temporal",
  "id_round1": 11,
  "id_round2": 47,
  "id_round3": 90
 },
 {
  "num": 4.2,
  "name": "Total de carpetas de investigación determinadas con abstención a investigar",
  "id_round1": 12,
  "id_round2": 48,
  "id_round3": 91
 },
 {
  "num": 4.3,
  "name": "Total de carpetas de investigación determinadas con no ejercicio de la acción penal",
  "id_round1": 13,
  "id_round2": 49,
  "id_round3": 92
 },
 {
  "num": 4.4,
  "name": "Total de carpetas de investigación determinadas con criterio de oportunidad",
  "id_round1": 14,
  "id_round2": 50,
  "id_round3": 93
 },
 {
  "num": "5.A",
  "name": "Total de carpetas de investigación resueltas mediante acuerdo reparatorio",
  "id_round1": 0,
  "id_round2": 51,
  "id_round3": 94
 },
 {
  "num": "5.A.1",
  "name": "Total de carpetas de investigación resueltas mediante acuerdo reparatorio en investigación inicial",
  "id_round1": 15,
  "id_round2": 52,
  "id_round3": 95
 },
 {
  "num": "5.A.2",
  "name": "Total de carpetas de investigación resueltas mediante acuerdo reparatorio en investigación complementaria",
  "id_round1": 16,
  "id_round2": 53,
  "id_round3": 96
 },
 {
  "num": "5.B",
  "name": "Total de carpetas de investigación derivadas al órgano MASC",
  "id_round1": 0,
  "id_round2": 54,
  "id_round3": 97
 },
 {
  "num": "5.B.1",
  "name": "Total de carpetas de investigación derivadas al órgano MASC en investigación inicial que fueron resueltas mediante acuerdo reparatorio",
  "id_round1": 17,
  "id_round2": 55,
  "id_round3": 98
 },
 {
  "num": "5.B.2",
  "name": "Total de carpetas de investigación derivadas al órgano MASC en investigación inicial que no fueron resueltas mediante acuerdo reparatorio",
  "id_round1": 0,
  "id_round2": 56,
  "id_round3": 99
 },
 {
  "num": 6,
  "name": "Total de carpetas de investigación a las que se formuló imputación",
  "id_round1": 18,
  "id_round2": 57,
  "id_round3": 100
 },
 {
  "num": 7,
  "name": " Total de carpetas de investigación vinculadas a proceso",
  "id_round1": 0,
  "id_round2": 58,
  "id_round3": 101
 },
 {
  "num": 8,
  "name": "Total de autos de vinculación a proceso dictados",
  "id_round1": 19,
  "id_round2": 59,
  "id_round3": 102
 },
 {
  "num": 9,
  "name": "Total de carpetas de investigación con al menos una medida cautelar",
  "id_round1": 20,
  "id_round2": 60,
  "id_round3": 103
 },
 {
  "num": 9.1,
  "name": "Total de carpetas de investigación con prisión preventiva",
  "id_round1": 21,
  "id_round2": 61,
  "id_round3": 104
 },
 {
  "num": "9.1.1",
  "name": "Total de carpetas de investigación con prisión preventiva oficiosa",
  "id_round1": 0,
  "id_round2": 62,
  "id_round3": 105
 },
 {
  "num": "9.1.2",
  "name": "Total de carpetas de investigación con prisión preventiva justificada",
  "id_round1": 0,
  "id_round2": 63,
  "id_round3": 106
 },
 {
  "num": 9.2,
  "name": "Total de carpetas de investigación con una medida cautelar distinta a prisión preventiva",
  "id_round1": 22,
  "id_round2": 64,
  "id_round3": 107
 },
 {
  "num": 10,
  "name": "Total de suspensiones condicionales del proceso",
  "id_round1": 23,
  "id_round2": 65,
  "id_round3": 108
 },
 {
  "num": 10.1,
  "name": "Total de investigaciones vinculadas a proceso resultas por suspensión condicional del proceso",
  "id_round1": 0,
  "id_round2": 66,
  "id_round3": 109
 },
 {
  "num": 11,
  "name": "Total de procedimientos abreviados",
  "id_round1": 24,
  "id_round2": 67,
  "id_round3": 110
 },
 {
  "num": 11.1,
  "name": "Delito 1",
  "id_round1": 25,
  "id_round2": 68,
  "id_round3": 111
 },
 {
  "num": 11.2,
  "name": "Delito 2",
  "id_round1": 26,
  "id_round2": 69,
  "id_round3": 112
 },
 {
  "num": 11.3,
  "name": "Delito 3",
  "id_round1": 27,
  "id_round2": 70,
  "id_round3": 113
 },
 {
  "num": 12,
  "name": "Total de acusaciones",
  "id_round1": 28,
  "id_round2": 71,
  "id_round3": 114
 },
 {
  "num": 13,
  "name": "Total de autos de apertura a juicio oral",
  "id_round1": 29,
  "id_round2": 72,
  "id_round3": 115
 },
 {
  "num": 14,
  "name": "Total de juicios orales",
  "id_round1": 30,
  "id_round2": 73,
  "id_round3": 116
 },
 {
  "num": 14.1,
  "name": "Total de juicios orales resueltos mediante sentencia absolutoria",
  "id_round1": 31,
  "id_round2": 74,
  "id_round3": 117
 },
 {
  "num": 14.2,
  "name": "Total de juicios orales resueltos mediante sentencia condenatoria",
  "id_round1": 32,
  "id_round2": 75,
  "id_round3": 118
 },
 {
  "num": 14.3,
  "name": "Total de juicios orales a los que se les dictó sobreseimiento",
  "id_round1": 33,
  "id_round2": 76,
  "id_round3": 119
 }
]';


      $a = DB::table('states')->join('quantitative_sources AS qs','qs.state_id','states.id')
      ->select('states.*','qs.data')
      ->where('states.name',$id)->where('states.round','<','4')->get();

      $c = null;
      $f = null;
      $g = null;
      if (count($a) == 1) {
        $c = [$a[0]->round,json_decode($a[0]->data)];
      }
      if (count($a) > 1 && count($a) < 3) {
        $c = [$a[0]->round,json_decode($a[0]->data)];
        $f = [$a[1]->round,json_decode($a[1]->data)];
      }
      if (count($a) > 2) {
        $c = [$a[0]->round,json_decode($a[0]->data)];
        $f = [$a[1]->round,json_decode($a[1]->data)];
        $g = [$a[2]->round,json_decode($a[2]->data)];
      }
      $json = json_decode($json_v);
      $b = [];
      foreach ($json as $key => $value) {
        $variables1 = DB::table('variables')->where('id',$value->id_round1)->first();
        $variables2 = DB::table('variables')->where('id',$value->id_round2)->first();
        $variables3 = DB::table('variables')->where('id',$value->id_round3)->first();

        $b [] = [$value,[$variables1,$variables2,$variables3]];
      }

      $d = [$c,$f,$g];

      $r_f1 = [];
      foreach ($b as $key => $value) {

          $data_uno_2017 = 'N.D';
          $data_uno_2018 = 'N.D';
          $data_uno_2019 = 'N.D';
          $data_uno_2020 = 'N.D';
          $data_uno_2021 = 'N.D';
          $data_uno_2022 = 'N.D';

          $variables_uno_2017_s = 'N.D';
          $variables_uno_2018_s = 'N.D';
          $variables_uno_2019_s = 'N.D';
          $variables_uno_2020_s = 'N.D';
          $variables_uno_2021_s = 'N.D';
          $variables_uno_2022_s = 'N.D';

          $variables_uno_2017_c = 'N.D';
          $variables_uno_2018_c = 'N.D';
          $variables_uno_2019_c = 'N.D';
          $variables_uno_2020_c = 'N.D';
          $variables_uno_2021_c = 'N.D';
          $variables_uno_2022_c = 'N.D';

          $data_dos_2017 = 'N.D';
          $data_dos_2018 = 'N.D';
          $data_dos_2019 = 'N.D';
          $data_dos_2020 = 'N.D';
          $data_dos_2021 = 'N.D';
          $data_dos_2022 = 'N.D';

          $variables_dos_2017_s = 'N.D';
          $variables_dos_2018_s = 'N.D';
          $variables_dos_2019_s = 'N.D';
          $variables_dos_2020_s = 'N.D';
          $variables_dos_2021_s = 'N.D';
          $variables_dos_2022_s = 'N.D';

          $variables_dos_2017_c = 'N.D';
          $variables_dos_2018_c = 'N.D';
          $variables_dos_2019_c = 'N.D';
          $variables_dos_2020_c = 'N.D';
          $variables_dos_2021_c = 'N.D';
          $variables_dos_2022_c = 'N.D';

          $data_tres_2017 = 'N.D';
          $data_tres_2018 = 'N.D';
          $data_tres_2019 = 'N.D';
          $data_tres_2020 = 'N.D';
          $data_tres_2021 = 'N.D';
          $data_tres_2022 = 'N.D';

          $variables_tres_2017_s = 'N.D';
          $variables_tres_2018_s = 'N.D';
          $variables_tres_2019_s = 'N.D';
          $variables_tres_2020_s = 'N.D';
          $variables_tres_2021_s = 'N.D';
          $variables_tres_2022_s = 'N.D';

          $variables_tres_2017_c = 'N.D';
          $variables_tres_2018_c = 'N.D';
          $variables_tres_2019_c = 'N.D';
          $variables_tres_2020_c = 'N.D';
          $variables_tres_2021_c = 'N.D';
          $variables_tres_2022_c = 'N.D';

          if ( !is_null($d[0]) && is_null($d[1]) && is_null($d[2]) ) {
            if ($d[0][0] == 1 ) {
              $variables_uno_2017_s = !is_null($value[1][0]) ? $value[1][0]->slug_2017: 'N.D.D';
              $variables_uno_2017_c = !is_null($value[1][0]) ? $value[1][0]->check_slug_2017: 'N.D.D';
              $variables_uno_2018_s = !is_null($value[1][0]) ? $value[1][0]->slug_2018: 'N.D.D';
              $variables_uno_2018_c = !is_null($value[1][0]) ? $value[1][0]->check_slug_2018: 'N.D.D';
              $variables_uno_2019_s = !is_null($value[1][0]) ? $value[1][0]->slug_2019: 'N.D.D';
              $variables_uno_2019_c = !is_null($value[1][0]) ? $value[1][0]->check_slug_2019: 'N.D.D';
              $variables_uno_2020_s = !is_null($value[1][0]) ? $value[1][0]->slug_2020: 'N.D.D';
              $variables_uno_2020_c = !is_null($value[1][0]) ? $value[1][0]->check_slug_2020: 'N.D.D';
              $variables_uno_2021_s = !is_null($value[1][0]) ? $value[1][0]->slug_2021: 'N.D.D';
              $variables_uno_2021_c = !is_null($value[1][0]) ? $value[1][0]->check_slug_2021: 'N.D.D';
              $variables_uno_2022_s = !is_null($value[1][0]) ? $value[1][0]->slug_2022: 'N.D.D';
              $variables_uno_2022_c = !is_null($value[1][0]) ? $value[1][0]->check_slug_2022: 'N.D.D';

              // $value[1][0]
            }
            if ($d[0][0] == 2 ) {
              // code...
              $variables_uno_2017_s = !is_null($value[1][1]) ? $value[1][1]->slug_2017: 'N.D.D';
              $variables_uno_2017_c = !is_null($value[1][1]) ? $value[1][1]->check_slug_2017: 'N.D.D';
              $variables_uno_2018_s = !is_null($value[1][1]) ? $value[1][1]->slug_2018: 'N.D.D';
              $variables_uno_2018_c = !is_null($value[1][1]) ? $value[1][1]->check_slug_2018: 'N.D.D';
              $variables_uno_2019_s = !is_null($value[1][1]) ? $value[1][1]->slug_2019: 'N.D.D';
              $variables_uno_2019_c = !is_null($value[1][1]) ? $value[1][1]->check_slug_2019: 'N.D.D';
              $variables_uno_2020_s = !is_null($value[1][1]) ? $value[1][1]->slug_2020: 'N.D.D';
              $variables_uno_2020_c = !is_null($value[1][1]) ? $value[1][1]->check_slug_2020: 'N.D.D';
              $variables_uno_2021_s = !is_null($value[1][1]) ? $value[1][1]->slug_2021: 'N.D.D';
              $variables_uno_2021_c = !is_null($value[1][1]) ? $value[1][1]->check_slug_2021: 'N.D.D';
              $variables_uno_2022_s = !is_null($value[1][1]) ? $value[1][1]->slug_2022: 'N.D.D';
              $variables_uno_2022_c = !is_null($value[1][1]) ? $value[1][1]->check_slug_2022: 'N.D.D';
              // $value[1][1]
            }
            if ($d[0][0] == 3 ) {
              // code...
              $variables_uno_2017_s = !is_null($value[1][2]) ? $value[1][2]->slug_2017: 'N.D.D';
              $variables_uno_2017_c = !is_null($value[1][2]) ? $value[1][2]->check_slug_2017: 'N.D.D';
              $variables_uno_2018_s = !is_null($value[1][2]) ? $value[1][2]->slug_2018: 'N.D.D';
              $variables_uno_2018_c = !is_null($value[1][2]) ? $value[1][2]->check_slug_2018: 'N.D.D';
              $variables_uno_2019_s = !is_null($value[1][2]) ? $value[1][2]->slug_2019: 'N.D.D';
              $variables_uno_2019_c = !is_null($value[1][2]) ? $value[1][2]->check_slug_2019: 'N.D.D';
              $variables_uno_2020_s = !is_null($value[1][2]) ? $value[1][2]->slug_2020: 'N.D.D';
              $variables_uno_2020_c = !is_null($value[1][2]) ? $value[1][2]->check_slug_2020: 'N.D.D';
              $variables_uno_2021_s = !is_null($value[1][2]) ? $value[1][2]->slug_2021: 'N.D.D';
              $variables_uno_2021_c = !is_null($value[1][2]) ? $value[1][2]->check_slug_2021: 'N.D.D';
              $variables_uno_2022_s = !is_null($value[1][2]) ? $value[1][2]->slug_2022: 'N.D.D';
              $variables_uno_2022_c = !is_null($value[1][2]) ? $value[1][2]->check_slug_2022: 'N.D.D';
              // $value[1][2]
            }
          }

          if ( !is_null($d[0]) && !is_null($d[1]) && is_null($d[2]) ) {
            $variables_uno_2017_s = !is_null($value[1][0]) ? $value[1][0]->slug_2017: 'N.D.D';
            $variables_uno_2017_c = !is_null($value[1][0]) ? $value[1][0]->check_slug_2017: 'N.D.D';
            $variables_uno_2018_s = !is_null($value[1][0]) ? $value[1][0]->slug_2018: 'N.D.D';
            $variables_uno_2018_c = !is_null($value[1][0]) ? $value[1][0]->check_slug_2018: 'N.D.D';
            $variables_uno_2019_s = !is_null($value[1][0]) ? $value[1][0]->slug_2019: 'N.D.D';
            $variables_uno_2019_c = !is_null($value[1][0]) ? $value[1][0]->check_slug_2019: 'N.D.D';
            $variables_uno_2020_s = !is_null($value[1][0]) ? $value[1][0]->slug_2020: 'N.D.D';
            $variables_uno_2020_c = !is_null($value[1][0]) ? $value[1][0]->check_slug_2020: 'N.D.D';
            $variables_uno_2021_s = !is_null($value[1][0]) ? $value[1][0]->slug_2021: 'N.D.D';
            $variables_uno_2021_c = !is_null($value[1][0]) ? $value[1][0]->check_slug_2021: 'N.D.D';
            $variables_uno_2022_s = !is_null($value[1][0]) ? $value[1][0]->slug_2022: 'N.D.D';
            $variables_uno_2022_c = !is_null($value[1][0]) ? $value[1][0]->check_slug_2022: 'N.D.D';

            $variables_dos_2017_s = !is_null($value[1][1]) ? $value[1][1]->slug_2017: 'N.D.D';
            $variables_dos_2017_c = !is_null($value[1][1]) ? $value[1][1]->check_slug_2017: 'N.D.D';
            $variables_dos_2018_s = !is_null($value[1][1]) ? $value[1][1]->slug_2018: 'N.D.D';
            $variables_dos_2018_c = !is_null($value[1][1]) ? $value[1][1]->check_slug_2018: 'N.D.D';
            $variables_dos_2019_s = !is_null($value[1][1]) ? $value[1][1]->slug_2019: 'N.D.D';
            $variables_dos_2019_c = !is_null($value[1][1]) ? $value[1][1]->check_slug_2019: 'N.D.D';
            $variables_dos_2020_s = !is_null($value[1][1]) ? $value[1][1]->slug_2020: 'N.D.D';
            $variables_dos_2020_c = !is_null($value[1][1]) ? $value[1][1]->check_slug_2020: 'N.D.D';
            $variables_dos_2021_s = !is_null($value[1][1]) ? $value[1][1]->slug_2021: 'N.D.D';
            $variables_dos_2021_c = !is_null($value[1][1]) ? $value[1][1]->check_slug_2021: 'N.D.D';
            $variables_dos_2022_s = !is_null($value[1][1]) ? $value[1][1]->slug_2022: 'N.D.D';
            $variables_dos_2022_c = !is_null($value[1][1]) ? $value[1][1]->check_slug_2022: 'N.D.D';
          }

          if ( !is_null($d[0]) && !is_null($d[1]) && !is_null($d[2]) ) {
            $variables_uno_2017_s = !is_null($value[1][0]) ? $value[1][0]->slug_2017: 'N.D.D';
            $variables_uno_2017_c = !is_null($value[1][0]) ? $value[1][0]->check_slug_2017: 'N.D.D';
            $variables_uno_2018_s = !is_null($value[1][0]) ? $value[1][0]->slug_2018: 'N.D.D';
            $variables_uno_2018_c = !is_null($value[1][0]) ? $value[1][0]->check_slug_2018: 'N.D.D';
            $variables_uno_2019_s = !is_null($value[1][0]) ? $value[1][0]->slug_2019: 'N.D.D';
            $variables_uno_2019_c = !is_null($value[1][0]) ? $value[1][0]->check_slug_2019: 'N.D.D';
            $variables_uno_2020_s = !is_null($value[1][0]) ? $value[1][0]->slug_2020: 'N.D.D';
            $variables_uno_2020_c = !is_null($value[1][0]) ? $value[1][0]->check_slug_2020: 'N.D.D';
            $variables_uno_2021_s = !is_null($value[1][0]) ? $value[1][0]->slug_2021: 'N.D.D';
            $variables_uno_2021_c = !is_null($value[1][0]) ? $value[1][0]->check_slug_2021: 'N.D.D';
            $variables_uno_2022_s = !is_null($value[1][0]) ? $value[1][0]->slug_2022: 'N.D.D';
            $variables_uno_2022_c = !is_null($value[1][0]) ? $value[1][0]->check_slug_2022: 'N.D.D';

            $variables_dos_2017_s = !is_null($value[1][1]) ? $value[1][1]->slug_2017: 'N.D.D';
            $variables_dos_2017_c = !is_null($value[1][1]) ? $value[1][1]->check_slug_2017: 'N.D.D';
            $variables_dos_2018_s = !is_null($value[1][1]) ? $value[1][1]->slug_2018: 'N.D.D';
            $variables_dos_2018_c = !is_null($value[1][1]) ? $value[1][1]->check_slug_2018: 'N.D.D';
            $variables_dos_2019_s = !is_null($value[1][1]) ? $value[1][1]->slug_2019: 'N.D.D';
            $variables_dos_2019_c = !is_null($value[1][1]) ? $value[1][1]->check_slug_2019: 'N.D.D';
            $variables_dos_2020_s = !is_null($value[1][1]) ? $value[1][1]->slug_2020: 'N.D.D';
            $variables_dos_2020_c = !is_null($value[1][1]) ? $value[1][1]->check_slug_2020: 'N.D.D';
            $variables_dos_2021_s = !is_null($value[1][1]) ? $value[1][1]->slug_2021: 'N.D.D';
            $variables_dos_2021_c = !is_null($value[1][1]) ? $value[1][1]->check_slug_2021: 'N.D.D';
            $variables_dos_2022_s = !is_null($value[1][1]) ? $value[1][1]->slug_2022: 'N.D.D';
            $variables_dos_2022_c = !is_null($value[1][1]) ? $value[1][1]->check_slug_2022: 'N.D.D';

            $variables_tres_2017_s = !is_null($value[1][2]) ? $value[1][2]->slug_2017: 'N.D.D';
            $variables_tres_2017_c = !is_null($value[1][2]) ? $value[1][2]->check_slug_2017: 'N.D.D';
            $variables_tres_2018_s = !is_null($value[1][2]) ? $value[1][2]->slug_2018: 'N.D.D';
            $variables_tres_2018_c = !is_null($value[1][2]) ? $value[1][2]->check_slug_2018: 'N.D.D';
            $variables_tres_2019_s = !is_null($value[1][2]) ? $value[1][2]->slug_2019: 'N.D.D';
            $variables_tres_2019_c = !is_null($value[1][2]) ? $value[1][2]->check_slug_2019: 'N.D.D';
            $variables_tres_2020_s = !is_null($value[1][2]) ? $value[1][2]->slug_2020: 'N.D.D';
            $variables_tres_2020_c = !is_null($value[1][2]) ? $value[1][2]->check_slug_2020: 'N.D.D';
            $variables_tres_2021_s = !is_null($value[1][2]) ? $value[1][2]->slug_2021: 'N.D.D';
            $variables_tres_2021_c = !is_null($value[1][2]) ? $value[1][2]->check_slug_2021: 'N.D.D';
            $variables_tres_2022_s = !is_null($value[1][2]) ? $value[1][2]->slug_2022: 'N.D.D';
            $variables_tres_2022_c = !is_null($value[1][2]) ? $value[1][2]->check_slug_2022: 'N.D.D';
          }
          // *************************************************************************
          if ( !is_null($d[0]) && is_null($d[1]) && is_null($d[2]) ) {
            if ($d[0][0] == 1 ) {
              if (isset($d[0][1]->$variables_uno_2017_c)) {
                $data_uno_2017 = is_numeric($d[0][1]->$variables_uno_2017_c) ? (($d[0][1]->$variables_uno_2017_c === 0) ?  $d[0][1]->$variables_uno_2017_s : 'N/A'  )  : 'S/D' ;
                $data_uno_2018 = is_numeric($d[0][1]->$variables_uno_2018_c) ? (($d[0][1]->$variables_uno_2018_c === 0) ?  $d[0][1]->$variables_uno_2018_s : 'N/A'  )  : 'S/D' ;
                $data_uno_2019 = is_numeric($d[0][1]->$variables_uno_2019_c) ? (($d[0][1]->$variables_uno_2019_c === 0) ?  $d[0][1]->$variables_uno_2019_s : 'N/A'  )  : 'S/D' ;
                $data_uno_2020 = is_numeric($d[0][1]->$variables_uno_2020_c) ? (($d[0][1]->$variables_uno_2020_c === 0) ?  $d[0][1]->$variables_uno_2020_s : 'N/A'  )  : 'S/D' ;
                $data_uno_2021 = is_numeric($d[0][1]->$variables_uno_2021_c) ? (($d[0][1]->$variables_uno_2021_c === 0) ?  $d[0][1]->$variables_uno_2021_s : 'N/A'  )  : 'S/D' ;
              }
              if (isset($d[0][1]->$variables_uno_2022_c)) {
                $data_uno_2022 = is_numeric($d[0][1]->$variables_uno_2022_c) ? (($d[0][1]->$variables_uno_2022_c === 0) ?  $d[0][1]->$variables_uno_2022_s : 'N/A'  )  : 'S/D' ;
              }
            }
            if ($d[0][0] == 2 ) {
              if (isset($d[0][1]->$variables_dos_2017_c)) {
                $data_dos_2017 = is_numeric($d[0][1]->$variables_uno_2017_c) ? (($d[0][1]->$variables_uno_2017_c === 0) ?  $d[0][1]->$variables_uno_2017_s : 'N/A'  )  : 'S/D' ;
              }
              $data_dos_2018 = is_numeric($d[0][1]->$variables_uno_2018_c) ? (($d[0][1]->$variables_uno_2018_c === 0) ?  $d[0][1]->$variables_uno_2018_s : 'N/A'  )  : 'S/D' ;
              $data_dos_2019 = is_numeric($d[0][1]->$variables_uno_2019_c) ? (($d[0][1]->$variables_uno_2019_c === 0) ?  $d[0][1]->$variables_uno_2019_s : 'N/A'  )  : 'S/D' ;
              $data_dos_2020 = is_numeric($d[0][1]->$variables_uno_2020_c) ? (($d[0][1]->$variables_uno_2020_c === 0) ?  $d[0][1]->$variables_uno_2020_s : 'N/A'  )  : 'S/D' ;
              $data_dos_2021 = is_numeric($d[0][1]->$variables_uno_2021_c) ? (($d[0][1]->$variables_uno_2021_c === 0) ?  $d[0][1]->$variables_uno_2021_s : 'N/A'  )  : 'S/D' ;
              if (isset($d[0][1]->$variables_uno_2022_c)) {
                $data_dos_2022 = is_numeric($d[0][1]->$variables_uno_2022_c) ? (($d[0][1]->$variables_uno_2022_c === 0) ?  $d[0][1]->$variables_uno_2022_s : 'N/A'  )  : 'S/D' ;
              }
            }
            if ($d[0][0] == 3 ) {
              if (isset($d[0][1]->$variables_tres_2017_c)) {
                $data_tres_2017 = is_numeric($d[0][1]->$variables_uno_2017_c) ? (($d[0][1]->$variables_uno_2017_c === 0) ?  $d[0][1]->$variables_uno_2017_s : 'N/A'  )  : 'S/D' ;
              }
              $data_tres_2018 = is_numeric($d[0][1]->$variables_uno_2018_c) ? (($d[0][1]->$variables_uno_2018_c === 0) ?  $d[0][1]->$variables_uno_2018_s : 'N/A'  )  : 'S/D' ;
              $data_tres_2019 = is_numeric($d[0][1]->$variables_uno_2019_c) ? (($d[0][1]->$variables_uno_2019_c === 0) ?  $d[0][1]->$variables_uno_2019_s : 'N/A'  )  : 'S/D' ;
              $data_tres_2020 = is_numeric($d[0][1]->$variables_uno_2020_c) ? (($d[0][1]->$variables_uno_2020_c === 0) ?  $d[0][1]->$variables_uno_2020_s : 'N/A'  )  : 'S/D' ;
              $data_tres_2021 = is_numeric($d[0][1]->$variables_uno_2021_c) ? (($d[0][1]->$variables_uno_2021_c === 0) ?  $d[0][1]->$variables_uno_2021_s : 'N/A'  )  : 'S/D' ;
              if (isset($d[0][1]->$variables_uno_2022_c)) {
                $data_tres_2022 = is_numeric($d[0][1]->$variables_uno_2022_c) ? (($d[0][1]->$variables_uno_2022_c === 0) ?  $d[0][1]->$variables_uno_2022_s : 'N/A'  )  : 'S/D' ;
              }
            }
          }

          if ( !is_null($d[0]) && !is_null($d[1]) && is_null($d[2]) ) {
            if (isset($d[0][1]->$variables_uno_2017_c)) {
              $data_uno_2017 = $variables_uno_2017_c != 'N.D.D' ? ( is_numeric($d[0][1]->$variables_uno_2017_c) ? (($d[0][1]->$variables_uno_2017_c === 0) ? $d[0][1]->$variables_uno_2017_s : 'N/A' ):'N/A' ): 'S/D';
            }
            $data_uno_2018 = $variables_uno_2018_c != 'N.D.D' ? ( is_numeric($d[0][1]->$variables_uno_2018_c) ? (($d[0][1]->$variables_uno_2018_c === 0) ? $d[0][1]->$variables_uno_2018_s : 'N/A' ):'N/A' ): 'S/D';
            $data_uno_2019 = $variables_uno_2019_c != 'N.D.D' ? ( is_numeric($d[0][1]->$variables_uno_2019_c) ? (($d[0][1]->$variables_uno_2019_c === 0) ? $d[0][1]->$variables_uno_2019_s : 'N/A' ):'N/A' ): 'S/D';
            $data_uno_2020 = $variables_uno_2020_c != 'N.D.D' ? ( is_numeric($d[0][1]->$variables_uno_2020_c) ? (($d[0][1]->$variables_uno_2020_c === 0) ? $d[0][1]->$variables_uno_2020_s : 'N/A' ):'N/A' ): 'S/D';
            $data_uno_2021 = $variables_uno_2021_c != 'N.D.D' ? ( is_numeric($d[0][1]->$variables_uno_2021_c) ? (($d[0][1]->$variables_uno_2021_c === 0) ? $d[0][1]->$variables_uno_2021_s : 'N/A' ):'N/A' ): 'S/D';
            if (isset($d[0][1]->$variables_uno_2022_c)) {
              $data_uno_2022 = $variables_uno_2022_c != 'N.D.D' ? ( is_numeric($d[0][1]->$variables_uno_2022_c) ? (($d[0][1]->$variables_uno_2022_c === 0) ? $d[0][1]->$variables_uno_2022_s : 'N/A' ):'N/A' ): 'S/D';
            }

            if (isset($d[0][1]->$variables_dos_2017_c)) {
              $data_dos_2017 = $variables_dos_2017_c != 'N.D.D' ? ( is_numeric($d[1][1]->$variables_dos_2017_c) ? (($d[1][1]->$variables_dos_2017_c === 0) ? $d[1][1]->$variables_dos_2017_s : 'N/A' ): 'N/A' ): 'S/D';
            }
            $data_dos_2018 = $variables_dos_2018_c != 'N.D.D' ? ( is_numeric($d[1][1]->$variables_dos_2018_c) ? (($d[1][1]->$variables_dos_2018_c === 0) ? $d[1][1]->$variables_dos_2018_s : 'N/A' ): 'N/A' ): 'S/D';
            $data_dos_2019 = $variables_dos_2019_c != 'N.D.D' ? ( is_numeric($d[1][1]->$variables_dos_2019_c) ? (($d[1][1]->$variables_dos_2019_c === 0) ? $d[1][1]->$variables_dos_2019_s : 'N/A' ): 'N/A' ): 'S/D';
            $data_dos_2020 = $variables_dos_2020_c != 'N.D.D' ? ( is_numeric($d[1][1]->$variables_dos_2020_c) ? (($d[1][1]->$variables_dos_2020_c === 0) ? $d[1][1]->$variables_dos_2020_s : 'N/A' ): 'N/A' ): 'S/D';
            $data_dos_2021 = $variables_dos_2021_c != 'N.D.D' ? ( is_numeric($d[1][1]->$variables_dos_2021_c) ? (($d[1][1]->$variables_dos_2021_c === 0) ? $d[1][1]->$variables_dos_2021_s : 'N/A' ): 'N/A' ): 'S/D';
            if (isset($d[1][1]->$variables_dos_2022_c)) {
              $data_dos_2022 = $variables_dos_2022_c != 'N.D.D' ? ( is_numeric($d[1][1]->$variables_dos_2022_c) ? (($d[1][1]->$variables_dos_2022_c === 0) ? $d[1][1]->$variables_dos_2022_s : 'N/A' ): 'N/A' ): 'S/D';
            }
          }

          if ( !is_null($d[0]) && !is_null($d[1]) && !is_null($d[2]) ) {
            if (isset($d[0][1]->$variables_uno_2017_c)) {
              $data_uno_2017 = $variables_uno_2017_c != 'N.D.D' ? ( is_numeric($d[0][1]->$variables_uno_2017_c) ? (($d[0][1]->$variables_uno_2017_c === 0) ? $d[0][1]->$variables_uno_2017_s : 'N/A' ): 'N/A' ): 'S/D';
            }
            $data_uno_2018 = $variables_uno_2018_c != 'N.D.D' ? ( is_numeric($d[0][1]->$variables_uno_2018_c) ? (($d[0][1]->$variables_uno_2018_c === 0) ? $d[0][1]->$variables_uno_2018_s : 'N/A' ): 'N/A' ): 'S/D';
            $data_uno_2019 = $variables_uno_2019_c != 'N.D.D' ? ( is_numeric($d[0][1]->$variables_uno_2019_c) ? (($d[0][1]->$variables_uno_2019_c === 0) ? $d[0][1]->$variables_uno_2019_s : 'N/A' ): 'N/A' ): 'S/D';
            $data_uno_2020 = $variables_uno_2020_c != 'N.D.D' ? ( is_numeric($d[0][1]->$variables_uno_2020_c) ? (($d[0][1]->$variables_uno_2020_c === 0) ? $d[0][1]->$variables_uno_2020_s : 'N/A' ): 'N/A' ): 'S/D';
            $data_uno_2021 = $variables_uno_2021_c != 'N.D.D' ? ( is_numeric($d[0][1]->$variables_uno_2021_c) ? (($d[0][1]->$variables_uno_2021_c === 0) ? $d[0][1]->$variables_uno_2021_s : 'N/A' ): 'N/A' ): 'S/D';
            if (isset($d[0][1]->$variables_uno_2022_c)) {
              $data_uno_2022 = $variables_uno_2022_c != 'N.D.D' ? ( is_numeric($d[0][1]->$variables_uno_2022_c) ? (($d[0][1]->$variables_uno_2022_c === 0) ? $d[0][1]->$variables_uno_2022_s : 'N/A' ): 'N/A' ): 'S/D';
            }

            if (isset($d[0][1]->$variables_dos_2017_c)) {
              $data_dos_2017 = $variables_dos_2017_c != 'N.D.D' ? ( is_numeric($d[1][1]->$variables_dos_2017_c) ? (($d[1][1]->$variables_dos_2017_c === 0) ? $d[1][1]->$variables_dos_2017_s : 'N/A' ): 'N/A' ): 'S/D';
            }
            $data_dos_2018 = $variables_dos_2018_c != 'N.D.D' ? ( is_numeric($d[1][1]->$variables_dos_2018_c) ? (($d[1][1]->$variables_dos_2018_c === 0) ? $d[1][1]->$variables_dos_2018_s : 'N/A' ): 'N/A' ): 'S/D';
            $data_dos_2019 = $variables_dos_2019_c != 'N.D.D' ? ( is_numeric($d[1][1]->$variables_dos_2019_c) ? (($d[1][1]->$variables_dos_2019_c === 0) ? $d[1][1]->$variables_dos_2019_s : 'N/A' ): 'N/A' ): 'S/D';
            $data_dos_2020 = $variables_dos_2020_c != 'N.D.D' ? ( is_numeric($d[1][1]->$variables_dos_2020_c) ? (($d[1][1]->$variables_dos_2020_c === 0) ? $d[1][1]->$variables_dos_2020_s : 'N/A' ): 'N/A' ): 'S/D';
            $data_dos_2021 = $variables_dos_2021_c != 'N.D.D' ? ( is_numeric($d[1][1]->$variables_dos_2021_c) ? (($d[1][1]->$variables_dos_2021_c === 0) ? $d[1][1]->$variables_dos_2021_s : 'N/A' ): 'N/A' ): 'S/D';
            if (isset($d[1][1]->$variables_dos_2022_c)) {
              $data_dos_2022 = $variables_dos_2022_c != 'N.D.D' ? ( is_numeric($d[1][1]->$variables_dos_2022_c) ? (($d[1][1]->$variables_dos_2022_c === 0) ? $d[1][1]->$variables_dos_2022_s : 'N/A' ): 'N/A' ): 'S/D';
            }

            if (isset($d[0][1]->$variables_tres_2017_c)) {
              $data_tres_2017 = $variables_tres_2017_c != 'N.D.D' ? ( is_numeric($d[2][1]->$variables_tres_2017_c) ? (($d[2][1]->$variables_tres_2017_c === 0) ? $d[2][1]->$variables_tres_2017_s : 'N/A' ): 'N/A' ): 'S/D';
            }
            $data_tres_2018 = $variables_tres_2018_c != 'N.D.D' ? ( is_numeric($d[2][1]->$variables_tres_2018_c) ? (($d[2][1]->$variables_tres_2018_c === 0) ? $d[2][1]->$variables_tres_2018_s : 'N/A' ): 'N/A' ): 'S/D';
            $data_tres_2019 = $variables_tres_2019_c != 'N.D.D' ? ( is_numeric($d[2][1]->$variables_tres_2019_c) ? (($d[2][1]->$variables_tres_2019_c === 0) ? $d[2][1]->$variables_tres_2019_s : 'N/A' ): 'N/A' ): 'S/D';
            $data_tres_2020 = $variables_tres_2020_c != 'N.D.D' ? ( is_numeric($d[2][1]->$variables_tres_2020_c) ? (($d[2][1]->$variables_tres_2020_c === 0) ? $d[2][1]->$variables_tres_2020_s : 'N/A' ): 'N/A' ): 'S/D';
            $data_tres_2021 = $variables_tres_2021_c != 'N.D.D' ? ( is_numeric($d[2][1]->$variables_tres_2021_c) ? (($d[2][1]->$variables_tres_2021_c === 0) ? $d[2][1]->$variables_tres_2021_s : 'N/A' ): 'N/A' ): 'S/D';
            if (isset($d[2][1]->$variables_tres_2022_c)) {
              $data_tres_2022 = $variables_tres_2022_c != 'N.D.D' ? ( is_numeric($d[2][1]->$variables_tres_2022_c) ? (($d[2][1]->$variables_tres_2022_c === 0) ? $d[2][1]->$variables_tres_2022_s : 'N/A' ): 'N/A' ): 'S/D';
            }
          }

          $df2017 = 'N/A';
          $df2018 = 'N/A';
          $df2019 = 'N/A';
          $df2020 = 'N/A';
          $df2021 = 'N/A';
          $df2022 = 'N/A';

        if (is_numeric($data_uno_2017) && !is_numeric($data_dos_2017) && !is_numeric($data_tres_2017)) {
          $df2017 = $data_uno_2017;
        }
        if (is_numeric($data_uno_2017) && is_numeric($data_dos_2017) && !is_numeric($data_tres_2017)) {
          $df2017 = $data_dos_2017;
        }
        if (is_numeric($data_uno_2017) && is_numeric($data_dos_2017) && is_numeric($data_tres_2017)) {
          $df2017 = $data_dos_2017;
        }
        if (!is_numeric($data_uno_2017) && is_numeric($data_dos_2017) && !is_numeric($data_tres_2017)) {
          $df2017 = $data_dos_2017;
        }
        if (!is_numeric($data_uno_2017) && !is_numeric($data_dos_2017) && is_numeric($data_tres_2017)) {
          $df2017 = $data_tres_2017;
        }
        // ******************
        if (is_numeric($data_uno_2018) && !is_numeric($data_dos_2018) && !is_numeric($data_tres_2018)) {
          $df2018 = $data_uno_2018;
        }
        if (is_numeric($data_uno_2018) && is_numeric($data_dos_2018) && !is_numeric($data_tres_2018)) {
          $df2018 = $data_dos_2018;
        }
        if (is_numeric($data_uno_2018) && is_numeric($data_dos_2018) && is_numeric($data_tres_2018)) {
          $df2018 = $data_dos_2018;
        }
        if (!is_numeric($data_uno_2018) && is_numeric($data_dos_2018) && !is_numeric($data_tres_2018)) {
          $df2018 = $data_dos_2018;
        }
        if (!is_numeric($data_uno_2018) && !is_numeric($data_dos_2018) && is_numeric($data_tres_2018)) {
          $df2018 = $data_tres_2018;
        }
        // ******************
        if (is_numeric($data_uno_2019) && !is_numeric($data_dos_2019) && !is_numeric($data_tres_2019)) {
          $df2019 = $data_uno_2019;
        }
        if (is_numeric($data_uno_2019) && is_numeric($data_dos_2019) && !is_numeric($data_tres_2019)) {
          $df2019 = $data_dos_2019;
        }
        if (is_numeric($data_uno_2019) && is_numeric($data_dos_2019) && is_numeric($data_tres_2019)) {
          $df2019 = $data_dos_2019;
        }
        if (!is_numeric($data_uno_2019) && is_numeric($data_dos_2019) && !is_numeric($data_tres_2019)) {
          $df2019 = $data_dos_2019;
        }
        if (!is_numeric($data_uno_2019) && !is_numeric($data_dos_2019) && is_numeric($data_tres_2019)) {
          $df2019 = $data_tres_2019;
        }
        // ******************
        if (is_numeric($data_uno_2020) && !is_numeric($data_dos_2020) && !is_numeric($data_tres_2020)) {
          $df2020 = $data_uno_2020;
        }
        if (is_numeric($data_uno_2020) && is_numeric($data_dos_2020) && !is_numeric($data_tres_2020)) {
          $df2020 = $data_dos_2020;
        }
        if (is_numeric($data_uno_2020) && is_numeric($data_dos_2020) && is_numeric($data_tres_2020)) {
          $df2020 = $data_dos_2020;
        }
        if (!is_numeric($data_uno_2020) && is_numeric($data_dos_2020) && !is_numeric($data_tres_2020)) {
          $df2020 = $data_dos_2020;
        }
        if (!is_numeric($data_uno_2020) && !is_numeric($data_dos_2020) && is_numeric($data_tres_2020)) {
          $df2020 = $data_tres_2020;
        }
        // ******************
        if (is_numeric($data_uno_2021) && !is_numeric($data_dos_2021) && !is_numeric($data_tres_2021)) {
          $df2021 = $data_uno_2021;
        }
        if (is_numeric($data_uno_2021) && is_numeric($data_dos_2021) && !is_numeric($data_tres_2021)) {
          $df2021 = $data_dos_2021;
        }
        if (is_numeric($data_uno_2021) && is_numeric($data_dos_2021) && is_numeric($data_tres_2021)) {
          $df2021 = $data_dos_2021;
        }
        if (!is_numeric($data_uno_2021) && is_numeric($data_dos_2021) && !is_numeric($data_tres_2021)) {
          $df2021 = $data_dos_2021;
        }
        if (!is_numeric($data_uno_2021) && !is_numeric($data_dos_2021) && is_numeric($data_tres_2021)) {
          $df2021 = $data_tres_2021;
        }
        // ******************
        if (is_numeric($data_uno_2022) && !is_numeric($data_dos_2022) && !is_numeric($data_tres_2022)) {
          $df2022 = $data_uno_2022;
        }
        if (is_numeric($data_uno_2022) && is_numeric($data_dos_2022) && !is_numeric($data_tres_2022)) {
          $df2022 = $data_dos_2022;
        }
        if (is_numeric($data_uno_2022) && is_numeric($data_dos_2022) && is_numeric($data_tres_2022)) {
          $df2022 = $data_dos_2022;
        }
        if (!is_numeric($data_uno_2022) && is_numeric($data_dos_2022) && !is_numeric($data_tres_2022)) {
          $df2022 = $data_dos_2022;
        }
        if (!is_numeric($data_uno_2022) && !is_numeric($data_dos_2022) && is_numeric($data_tres_2022)) {
          $df2022 = $data_tres_2022;
        }
        // ******************

        // $data_uno_2017_f = $data_uno_2017
        $r_f1[] = [$id,$value[0]->num,$value[0]->name,$df2017,$df2018,$df2019,$df2020,$df2021,$df2022 ];


      }

      return response()->json($r_f1);

    // $sata = DB::table('states')->where('id',$id)->first();
    //
    // $idndic = DB::table('variables')->where('round',$sata->round)->get();
    // $data = DB::table('quantitative_sources')->where('state_id',$id)->first();
    // $json = json_decode($data->data);
    //
    // $datas = [];
    //
    // foreach ($idndic as $key => $value) {
    //   $data_dos_2017 = 'N.D';
    //   $data_dos_2018 = 'N.D';
    //   $data_dos_2019 = 'N.D';
    //   $data_dos_2020 = 'N.D';
    //   $data_dos_2021 = 'N.D';
    //   $data_dos_2022 = 'N.D';
    //   foreach ($json as $keyd => $valued) {
    //     if ($value->check_slug_2017 === $keyd) {
    //       if ($valued == 1) {
    //         $data_dos_2017 =  'N/A';
    //       }
    //     }
    //     if ($value->slug_2017 === $keyd) {
    //       if ($valued != null) {
    //         $data_dos_2017 =  $valued;
    //       }
    //     }
    //
    //     if ($value->check_slug_2018 === $keyd) {
    //       if ($valued == 1) {
    //         $data_dos_2018 =   'N/A';
    //       }
    //     }
    //     if ($value->slug_2018 === $keyd) {
    //       if ($valued != null) {
    //         $data_dos_2018 =  $valued;
    //       }
    //     }
    //
    //     if ($value->check_slug_2019 === $keyd) {
    //       if ($valued == 1) {
    //         $data_dos_2019 =  'N/A';
    //       }
    //     }
    //     if ($value->slug_2019 === $keyd) {
    //       if ($valued != null) {
    //         $data_dos_2019 =  $valued;
    //       }
    //     }
    //
    //     if ($value->check_slug_2020 === $keyd) {
    //       if ($valued == 1) {
    //         $data_dos_2020 =  'N/A';
    //       }
    //     }
    //     if ($value->slug_2020 === $keyd) {
    //       if ($valued != null) {
    //         $data_dos_2020 =  $valued;
    //       }
    //     }
    //
    //     if ($value->check_slug_2021 === $keyd) {
    //       if ($valued == 1) {
    //         $data_dos_2021 =  'N/A';
    //       }
    //     }
    //     if ($value->slug_2021 === $keyd) {
    //       if ($valued != null) {
    //         $data_dos_2021 =  $valued;
    //       }
    //     }
    //
    //     if ($value->check_slug_2022 === $keyd) {
    //       if ($valued == 1) {
    //         $data_dos_2022 =  'N/A';
    //       }
    //     }
    //     if ($value->slug_2022 === $keyd) {
    //       if ($valued != null) {
    //         $data_dos_2022 =  $valued;
    //       }
    //     }
    //
    //
    //   }
    //
    //   // $datas [] = [$value->num, $value->name,$data_dos[0],$data_dos[1],$data_dos[2],$data_dos[3],$data_dos[4]];
    //   $datas [] = [$sata->name,$sata->round,$value->num, $value->name,$data_dos_2017,$data_dos_2018,$data_dos_2019,$data_dos_2020,$data_dos_2021,$data_dos_2022];
    // }
    //
    //
    // return response()->json($datas);

    // $states = DB::table('states')->where('round','2')->get();
    // $d = [];
    // $years = ['2018','2019','2020','2021'];
    // foreach ($states as $key => $value_s) {
    //   $data = DB::table('quantitative_sources')->where('state_id',$value_s->id)->first();
    //   $json = (array) json_decode($data->data);
    //   foreach ($idndic as $key => $value) {
    //     foreach ($years as $key => $value_y) {
    //       $dividend = explode('_',$value->dividend);
    //       $dividend = $dividend[0].'_'.$value_y;
    //       $divider = explode('_',$value->divider);
    //       $divider = $divider[0].'_'.$value_y;
    //       if ($value->num != '5.1') {
    //         if (is_null($json[$dividend]) || $json[$dividend] == 0 ) {
    //           $d [] = ['state'=> $value_s->name,'num' => $value->num,'dividend' => $json[$dividend], 'divider' => $json[$divider], 'r' => 'N/D', 'year' => $value_y];
    //         }elseif (is_null($json[$divider]) || $json[$divider] == 0) {
    //           $d [] = ['state'=> $value_s->name,'num' => $value->num,'dividend' => $json[$dividend], 'divider' => $json[$divider], 'r' => 'N/D', 'year' => $value_y];
    //         }else {
    //           $d [] = ['state'=> $value_s->name,
    //                    'num' => $value->num,
    //                    'dividend' => $json[$dividend],
    //                    'divider' => $json[$divider],
    //                    'r' => number_format((($json[$dividend] / $json[$divider])*100),1,'.',' '), 'year' => $value_y];
    //         }
    //       }
    //     }
    //   }
    // }
    //
    // return response()->json($d);
  }

}
