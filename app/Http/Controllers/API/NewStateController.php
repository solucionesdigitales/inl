<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\State;
use App\DataLoaded;
use App\User;
use App\Interpretation;
use App\QuantitativeSource;
use Illuminate\Support\Facades\DB;
use Mail;
use App\AllHeadquarters;
use App\Headquarter;
use App\Problematic;
use App\Conclusion;
use App\Criterionproblematic;
use App\Indicator;
use App\Observation;

class NewStateController extends Controller
{
  public function createState(Request $request)
  {
    // $ultimoID = DB::table('quantitative_sources')->max('id');

    // return response()->json($ultimoID);

    $state_f = State::where('name','LIKE','%'.$request->name.'%')->where('round','3')->first();
    if (isset($state_f)) {
      return response()->json(['status' => false]);
    }

    $abrev = strtolower(substr($request->name, 0 , 3));

    $state = new State;
    $state->name = $request->name;
    $state->key = $abrev.'3';
    $state->round = 3;
    $state->save();

    $valorGeneral = '{
      "uno": {"total_amount" : "1","type" : "map"},
      "dos": {"total_amount" : "1","type" : "org"},
      "tres": {"total_amount" : "1","type" : "head"},
      "cuatro": {"total_amount" : "129","type" : "cuanti"},
      "cinco": {"total_amount" : "149","type" : "conclusions"},
      "seis": {"total_amount" : "149","type" : "problematics"},
      "seite": {"total_amount" : "1045","type" : "observations"},
      "ocho": {"total_amount" : "150","type" : "criterion_conclusion"}
    }';
    $valorGeneral = json_decode($valorGeneral);

    foreach ($valorGeneral as $key => $value_data) {
      $dataLoade = new DataLoaded;
      $dataLoade->state_id = $state->id;
      $dataLoade->percentage = 0;
      $dataLoade->total_amount = $value_data->total_amount;
      $dataLoade->registered_amount = 0;
      $dataLoade->type = $value_data->type;
      $dataLoade->save();
    }

    $ultimoID = DB::table('quantitative_sources')->max('id');

    $cuanti ='{"id": '.($ultimoID + 1).', "state_id": '.$state->id.', "checked1_2017": 0, "checked1_2018": 0, "checked1_2019": 0, "checked1_2020": 0, "checked1_2021": 0, "checked1_2022": 0, "checked2_2017": 0, "checked2_2018": 0, "checked2_2019": 0, "checked2_2020": 0, "checked2_2021": 0, "checked2_2022": 0, "checked3_2017": 0, "checked3_2018": 0, "checked3_2019": 0, "checked3_2020": 0, "checked3_2021": 0, "checked3_2022": 0, "checked4_2017": 0, "checked4_2018": 0, "checked4_2019": 0, "checked4_2020": 0, "checked4_2021": 0, "checked4_2022": 0, "checked5_2017": 0, "checked5_2018": 0, "checked5_2019": 0, "checked5_2020": 0, "checked5_2021": 0, "checked5_2022": 0, "checked6_2017": 0, "checked6_2018": 0, "checked6_2019": 0, "checked6_2020": 0, "checked6_2021": 0, "checked6_2022": 0, "checked7_2017": 0, "checked7_2018": 0, "checked7_2019": 0, "checked7_2020": 0, "checked7_2021": 0, "checked7_2022": 0, "checked8_2017": 0, "checked8_2018": 0, "checked8_2019": 0, "checked8_2020": 0, "checked8_2021": 0, "checked8_2022": 0, "checked9_2017": 0, "checked9_2018": 0, "checked9_2019": 0, "checked9_2020": 0, "checked9_2021": 0, "checked9_2022": 0, "checked10_2017": 0, "checked10_2018": 0, "checked10_2019": 0, "checked10_2020": 0, "checked10_2021": 0, "checked10_2022": 0, "checked11_2017": 0, "checked11_2018": 0, "checked11_2019": 0, "checked11_2020": 0, "checked11_2021": 0, "checked11_2022": 0, "checked12_2017": 0, "checked12_2018": 0, "checked12_2019": 0, "checked12_2020": 0, "checked12_2021": 0, "checked12_2022": 0, "checked13_2017": 0, "checked13_2018": 0, "checked13_2019": 0, "checked13_2020": 0, "checked13_2021": 0, "checked13_2022": 0, "checked14_2017": 0, "checked14_2018": 0, "checked14_2019": 0, "checked14_2020": 0, "checked14_2021": 0, "checked14_2022": 0, "checked15_2017": 0, "checked15_2018": 0, "checked15_2019": 0, "checked15_2020": 0, "checked15_2021": 0, "checked15_2022": 0, "checked16_2017": 0, "checked16_2018": 0, "checked16_2019": 0, "checked16_2020": 0, "checked16_2021": 0, "checked16_2022": 0, "checked17_2017": 0, "checked17_2018": 0, "checked17_2019": 0, "checked17_2020": 0, "checked17_2021": 0, "checked17_2022": 0, "checked18_2017": 0, "checked18_2018": 0, "checked18_2019": 0, "checked18_2020": 0, "checked18_2021": 0, "checked18_2022": 0, "checked19_2017": 0, "checked19_2018": 0, "checked19_2019": 0, "checked19_2020": 0, "checked19_2021": 0, "checked19_2022": 0, "checked20_2017": 0, "checked20_2018": 0, "checked20_2019": 0, "checked20_2020": 0, "checked20_2021": 0, "checked20_2022": 0, "checked21_2017": 0, "checked21_2018": 0, "checked21_2019": 0, "checked21_2020": 0, "checked21_2021": 0, "checked21_2022": 0, "checked22_2017": 0, "checked22_2018": 0, "checked22_2019": 0, "checked22_2020": 0, "checked22_2021": 0, "checked22_2022": 0, "checked23_2017": 0, "checked23_2018": 0, "checked23_2019": 0, "checked23_2020": 0, "checked23_2021": 0, "checked23_2022": 0, "checked24_2017": 0, "checked24_2018": 0, "checked24_2019": 0, "checked24_2020": 0, "checked24_2021": 0, "checked24_2022": 0, "checked25_2017": 0, "checked25_2018": 0, "checked25_2019": 0, "checked25_2020": 0, "checked25_2021": 0, "checked25_2022": 0, "checked26_2017": 0, "checked26_2018": 0, "checked26_2019": 0, "checked26_2020": 0, "checked26_2021": 0, "checked26_2022": 0, "checked27_2017": 0, "checked27_2018": 0, "checked27_2019": 0, "checked27_2020": 0, "checked27_2021": 0, "checked27_2022": 0, "checked28_2017": 0, "checked28_2018": 0, "checked28_2019": 0, "checked28_2020": 0, "checked28_2021": 0, "checked28_2022": 0, "checked29_2017": 0, "checked29_2018": 0, "checked29_2019": 0, "checked29_2020": 0, "checked29_2021": 0, "checked29_2022": 0, "checked30_2017": 0, "checked30_2018": 0, "checked30_2019": 0, "checked30_2020": 0, "checked30_2021": 0, "checked30_2022": 0, "checked31_2017": 0, "checked31_2018": 0, "checked31_2019": 0, "checked31_2020": 0, "checked31_2021": 0, "checked31_2022": 0, "checked32_2017": 0, "checked32_2018": 0, "checked32_2019": 0, "checked32_2020": 0, "checked32_2021": 0, "checked32_2022": 0, "checked33_2017": 0, "checked33_2018": 0, "checked33_2019": 0, "checked33_2020": 0, "checked33_2021": 0, "checked33_2022": 0, "checked34_2017": 0, "checked34_2018": 0, "checked34_2019": 0, "checked34_2020": 0, "checked34_2021": 0, "checked34_2022": 0, "checked35_2017": 0, "checked35_2018": 0, "checked35_2019": 0, "checked35_2020": 0, "checked35_2021": 0, "checked35_2022": 0, "checked36_2017": 0, "checked36_2018": 0, "checked36_2019": 0, "checked36_2020": 0, "checked36_2021": 0, "checked36_2022": 0, "checked37_2017": 0, "checked37_2018": 0, "checked37_2019": 0, "checked37_2020": 0, "checked37_2021": 0, "checked37_2022": 0, "checked38_2017": 0, "checked38_2018": 0, "checked38_2019": 0, "checked38_2020": 0, "checked38_2021": 0, "checked38_2022": 0, "checked39_2017": 0, "checked39_2018": 0, "checked39_2019": 0, "checked39_2020": 0, "checked39_2021": 0, "checked39_2022": 0, "checked40_2017": 0, "checked40_2018": 0, "checked40_2019": 0, "checked40_2020": 0, "checked40_2021": 0, "checked40_2022": 0, "checked41_2017": 0, "checked41_2018": 0, "checked41_2019": 0, "checked41_2020": 0, "checked41_2021": 0, "checked41_2022": 0, "checked42_2017": 0, "checked42_2018": 0, "checked42_2019": 0, "checked42_2020": 0, "checked42_2021": 0, "checked42_2022": 0, "checked43_2017": 0, "checked43_2018": 0, "checked43_2019": 0, "checked43_2020": 0, "checked43_2021": 0, "checked43_2022": 0, "variable1_2017": null, "variable1_2018": null, "variable1_2019": null, "variable1_2020": null, "variable1_2021": null, "variable1_2022": null, "variable2_2017": null, "variable2_2018": null, "variable2_2019": null, "variable2_2020": null, "variable2_2021": null, "variable2_2022": null, "variable3_2017": null, "variable3_2018": null, "variable3_2019": null, "variable3_2020": null, "variable3_2021": null, "variable3_2022": null, "variable4_2017": null, "variable4_2018": null, "variable4_2019": null, "variable4_2020": null, "variable4_2021": null, "variable4_2022": null, "variable5_2017": null, "variable5_2018": null, "variable5_2019": null, "variable5_2020": null, "variable5_2021": null, "variable5_2022": null, "variable6_2017": null, "variable6_2018": null, "variable6_2019": null, "variable6_2020": null, "variable6_2021": null, "variable6_2022": null, "variable7_2017": null, "variable7_2018": null, "variable7_2019": null, "variable7_2020": null, "variable7_2021": null, "variable7_2022": null, "variable8_2017": null, "variable8_2018": null, "variable8_2019": null, "variable8_2020": null, "variable8_2021": null, "variable8_2022": null, "variable9_2017": null, "variable9_2018": null, "variable9_2019": null, "variable9_2020": null, "variable9_2021": null, "variable9_2022": null, "variable10_2017": null, "variable10_2018": null, "variable10_2019": null, "variable10_2020": null, "variable10_2021": null, "variable10_2022": null, "variable11_2017": null, "variable11_2018": null, "variable11_2019": null, "variable11_2020": null, "variable11_2021": null, "variable11_2022": null, "variable12_2017": null, "variable12_2018": null, "variable12_2019": null, "variable12_2020": null, "variable12_2021": null, "variable12_2022": null, "variable13_2017": null, "variable13_2018": null, "variable13_2019": null, "variable13_2020": null, "variable13_2021": null, "variable13_2022": null, "variable14_2017": null, "variable14_2018": null, "variable14_2019": null, "variable14_2020": null, "variable14_2021": null, "variable14_2022": null, "variable15_2017": null, "variable15_2018": null, "variable15_2019": null, "variable15_2020": null, "variable15_2021": null, "variable15_2022": null, "variable16_2017": null, "variable16_2018": null, "variable16_2019": null, "variable16_2020": null, "variable16_2021": null, "variable16_2022": null, "variable17_2017": null, "variable17_2018": null, "variable17_2019": null, "variable17_2020": null, "variable17_2021": null, "variable17_2022": null, "variable18_2017": null, "variable18_2018": null, "variable18_2019": null, "variable18_2020": null, "variable18_2021": null, "variable18_2022": null, "variable19_2017": null, "variable19_2018": null, "variable19_2019": null, "variable19_2020": null, "variable19_2021": null, "variable19_2022": null, "variable20_2017": null, "variable20_2018": null, "variable20_2019": null, "variable20_2020": null, "variable20_2021": null, "variable20_2022": null, "variable21_2017": null, "variable21_2018": null, "variable21_2019": null, "variable21_2020": null, "variable21_2021": null, "variable21_2022": null, "variable22_2017": null, "variable22_2018": null, "variable22_2019": null, "variable22_2020": null, "variable22_2021": null, "variable22_2022": null, "variable23_2017": null, "variable23_2018": null, "variable23_2019": null, "variable23_2020": null, "variable23_2021": null, "variable23_2022": null, "variable24_2017": null, "variable24_2018": null, "variable24_2019": null, "variable24_2020": null, "variable24_2021": null, "variable24_2022": null, "variable25_2017": null, "variable25_2018": null, "variable25_2019": null, "variable25_2020": null, "variable25_2021": null, "variable25_2022": null, "variable26_2017": null, "variable26_2018": null, "variable26_2019": null, "variable26_2020": null, "variable26_2021": null, "variable26_2022": null, "variable27_2017": null, "variable27_2018": null, "variable27_2019": null, "variable27_2020": null, "variable27_2021": null, "variable27_2022": null, "variable28_2017": null, "variable28_2018": null, "variable28_2019": null, "variable28_2020": null, "variable28_2021": null, "variable28_2022": null, "variable29_2017": null, "variable29_2018": null, "variable29_2019": null, "variable29_2020": null, "variable29_2021": null, "variable29_2022": null, "variable30_2017": null, "variable30_2018": null, "variable30_2019": null, "variable30_2020": null, "variable30_2021": null, "variable30_2022": null, "variable31_2017": null, "variable31_2018": null, "variable31_2019": null, "variable31_2020": null, "variable31_2021": null, "variable31_2022": null, "variable32_2017": null, "variable32_2018": null, "variable32_2019": null, "variable32_2020": null, "variable32_2021": null, "variable32_2022": null, "variable33_2017": null, "variable33_2018": null, "variable33_2019": null, "variable33_2020": null, "variable33_2021": null, "variable33_2022": null, "variable34_2017": null, "variable34_2018": null, "variable34_2019": null, "variable34_2020": null, "variable34_2021": null, "variable34_2022": null, "variable35_2017": null, "variable35_2018": null, "variable35_2019": null, "variable35_2020": null, "variable35_2021": null, "variable35_2022": null, "variable36_2017": null, "variable36_2018": null, "variable36_2019": null, "variable36_2020": null, "variable36_2021": null, "variable36_2022": null, "variable37_2017": null, "variable37_2018": null, "variable37_2019": null, "variable37_2020": null, "variable37_2021": null, "variable37_2022": null, "variable38_2017": null, "variable38_2018": null, "variable38_2019": null, "variable38_2020": null, "variable38_2021": null, "variable38_2022": null, "variable39_2017": null, "variable39_2018": null, "variable39_2019": null, "variable39_2020": null, "variable39_2021": null, "variable39_2022": null, "variable40_2017": null, "variable40_2018": null, "variable40_2019": null, "variable40_2020": null, "variable40_2021": null, "variable40_2022": null, "variable41_2017": null, "variable41_2018": null, "variable41_2019": null, "variable41_2020": null, "variable41_2021": null, "variable41_2022": null, "variable42_2017": null, "variable42_2018": null, "variable42_2019": null, "variable42_2020": null, "variable42_2021": null, "variable42_2022": null, "variable43_2017": null, "variable43_2018": null, "variable43_2019": null, "variable43_2020": null, "variable43_2021": null, "variable43_2022": null}';
    $cuanti = json_decode($cuanti);

    $qs = new QuantitativeSource();
    $qs->state_id = $state->id;
    $qs->data = json_encode($cuanti);
    $qs->save();

    $ultimoIDin = DB::table('interpretations')->max('id');

    $indata = '{
      "id" : '.($ultimoIDin + 1).',
      "state_id": '.$state->id.'
    }';
    $indata = json_decode($indata);

    $in = new Interpretation();
    $in->state_id = $state->id;
    $in->data = json_encode($indata);
    $in->save();

    $pro = new Problematic;
    $pro->state_id = $state->id;
    $pro->save();

    $con = new Conclusion;
    $con->state_id = $state->id;
    $con->save();

    $crpr = new Criterionproblematic;
    $crpr->state_id = $state->id;
    $crpr->save();

    $state_round_two = State::where('round','2')->select('id')->first();
    $observation_data = Observation::where('state_id', $state_round_two->id)->get();

    // $headquarter = Headquarter::where('state_id')->first();
    //
    // foreach ($observation_data as $keyod => $valueod) {
    //   $newObsevation = new Observation;
    //   $newObsevation->state_id = $state->id;
    //   $newObsevation->head
    //
    // }



    return response()->json(['status' => true]);
    // code...
  }

  public function updateState(Request $request)
  {
    $state_f = State::where('name','LIKE','%'.$request->name.'%')->where('round','3')->where('id','!=',$request->id)->first();

    if (isset($state_f)) {
      return response()->json(['status' => false]);
    }

    $state = State::where('id',$request->id)->first();
    $state->name = $request->name;
    $state->save();

    return response()->json(['status' => true]);
    // code...
  }

  public function changeHO($id)
  {
    $data = explode('&',$id);
    $all = AllHeadquarters::where('id',$data[0])->first();
    $all->evaluate = 1;
    $all->save();

    $head = new Headquarter;
    $head->state_id = $all->state_id;
    $head->name = $all->name;
    $head->city_halls = $all->city_halls;
    $head->agents = $all->agents;
    $head->save();

    DB::table('observations')->where('state_id', $all->state_id)->update(['headquarter_id' => $head->id]);

    return response()->json(['status' => true]);

  }

  public function unchangeHO($id)
  {
    $data = explode('&',$id);
    $all = AllHeadquarters::where('id',$data[0])->first();
    $all->evaluate = 0;
    $all->save();

    $head = Headquarter::where('state_id',$all->state_id)->where('name',$all->name)->delete();

    return response()->json(['status' => true]);

  }

  public function stateGetRules()
  {
    $rules = DB::table('rules_interpretation')
    ->join('states','rules_interpretation.key','states.key')
    ->select('rules_interpretation.*','states.id AS stateid')
    ->where('id_i','>', 75)->get();
    return response()->json($rules);
  }

  public function createStateRule(Request $request)
  {
    $state = State::where('id',$request->stateid)->first();
    $ind = Indicator::where('id', '>', 75)->where('num',$request->indicador)->first();
    DB::table('rules_interpretation')->insert([
      'state' => $state->name,
      'key' => $state->key,
      'year' => $request->year,
      'indicador' => $ind->num,
      'text' => $request->text,
      'id_i' => $ind->id
    ]);
    return response()->json(['status' => true]);
    // code...
  }

  public function updateStateRule(Request $request)
  {
    $state = State::where('id',$request->stateid)->first();
    $ind = Indicator::where('id', '>', 75)->where('num',$request->indicador)->first();

    DB::table('rules_interpretation')
    ->where('id', $request->id)
    ->update([
      'state' => $state->name,
      'key' => $state->key,
      'year' => $request->year,
      'indicador' => $ind->num,
      'text' => $request->text,
      'id_i' => $ind->id
    ]);

    return response()->json(['status' => true]);

  }

  public function deleteStateRule($id)
  {
    DB::table('rules_interpretation')
    ->where('id', $id)
    ->delete();

    return response()->json(['status' => true]);
  }
}
