# PROCESS MAPPING FOR STATE PROSECUTORS TOOL
[![version](https://img.shields.io/badge/versi%C3%B3n-1.2.0-blue.svg)](https://semver.org)

With the objectives of monitoring the justice system reform’s progress within the different Attorney General Offices (“AGOs”) in the Mexican states, assessing the adequate implementation of Mexican Prosecutors’ newly-assigned roles and responsibilities, and documenting the effective targeting of the United States Government’s investments in this reform, in 2017-2018 the Bureau of International Narcotics and Law Enforcement Affairs Office in Mexico (hereafter, INL/Mexico) developed the Process Mapping for State Prosecutors Tool (henceforth, “the Tool”).

The Tool provides information regarding how and in what context Mexican Prosecutors (Agentes del Ministerio Público or AMP in Spanish) make key decisions within the Accusatorial Criminal Justice System (ACJS); and the factors that lead to these decisions. Also, the Tool pinpoints the substantive processes followed by State Prosecutors under Mexico's ACJS and identifies performance indicators and efficiency gaps along the processes.

In 2019, INL/Mexico selected C230 Consultores1, in consortium with Fortis Consultoría2, to implement the Tool under contract number 19AQMM19C0039, awarded on March 3, 2019. The Extension Period of the Contract started on March 4th, 2023, and ended on September 2nd, 2023

The objective of the implementation of the Tool was to gather relevant data to obtain qualitative and quantitative information on thirteen selected key decisions that correspond to three levels of public policy decision-making (regulatory, institutional and individual) within state AGOs in Mexico and analyze the results to make recommendations regarding potential improvements to the decision-making process and public policy. To reach said objective, the Team obtained documentary, qualitative, and quantitative information regarding areas of opportunity within state AGOs in Mexico.

Based on the information obtained, the Team produced Detailed and Executive-level Reports for the AGOs that chose to participate in the project. At the end of the project, 31 states agreed3 to, at least, one round of Tool implementation, while others had up to three rounds4. Some allowed the implementation of the Tool on Specialized Attorney’s Offices5.

## :black_nib: Programming languages used

1. Back-end: The Digital Team programmed the back-end of the system using PHP, an open source programming language widely used in web development. The Laravel framework provided structure and tools to speed up the development of web applications.

- PHP version: 7.4
- Framework: Laravel 8.83.27
- Database: MySQL for data storage
- API: RESTful API provided communication between front-end and other external systems.
- Security: Password encryption and data validation implemented.


2. Front-end: The Digital Team programmed the front-end of the system using Vue.js, a progressive JavaScript framework for building interactive user interfaces. Vue.js allows to create reusable components and facilitates the manipulation of the DOM efficiently.

- Vue.js version: 2.6
- Package Management: npm is used for dependency management.
- Styling: CSS and Bootstrap framework were used for user interface's design and styling
- Back-end communication: HTTP requests were made through 


## 📋 System Requirements

- [ ] PHP 8 or lower
- [ ] Node 14 or lower


## :computer: Local Installation of the Application

- Run ` git clone https://gitlab.com/solucionesdigitales/inl `
- ` cd ` into the project folder.
- Run ` composer install `
- Create ` .env ` file from ` .env.example ` file and add local database information.
- Run ` php artisan key:generate ` to generate an API key.
- Run ` npm install ` or ` yarn install `
- Run ` php artisan migrate ` to migrate the database models.
- Run ` php artisan db:seed ` to add seeder data to the local database.
- Run ` php artisan passport:install ` to install passport keys.


## 📦 Packages or Dependencies Used

* [axios](https://github.com/axios/axios)
* [boostrap](https://getbootstrap.com/docs/4.0/getting-started/introduction/)
* [cross-env](https://www.npmjs.com/package/cross-env)
* [jquery](https://jquery.com/)
* [laravel-mix](https://laravel.com/docs/7.8/mix)
* [lodash](https://lodash.com/)
* [popper.js](https://popper.js.org/index.html)
* [vue](https://vuejs.org/)
* [fontawesome](https://fontawesome.com/)
* [admin-lte](https://adminlte.io/themes/dev/AdminLTE/index.html)
* [laravel-vue-pagination](https://github.com/gilbitron/laravel-vue-pagination)
* [moment](https://momentjs.com/)
* [sweetalert2](https://sweetalert2.github.io/)
* [vform](https://github.com/cretueusebiu/vform)
* [vue-multiselect](https://vue-multiselect.js.org/)
* [vue-progressbar](https://github.com/hilongjw/vue-progressbar)
* [vue-router](https://router.vuejs.org/)
* [vue-i18n](http://kazupon.github.io/vue-i18n/)
* [laravel-passport](https://laravel.com/docs/5.8/passport)
* [laravel-excel](https://laravel-excel.com/)